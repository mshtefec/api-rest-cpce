<?php

namespace App\Entity;

use App\Repository\AfiliadoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AfiliadoRepository::class)
 */
class Afiliado implements \JsonSerializable
{

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $afiNombre;

    /**
     * @ORM\Column(type="datetime")
     */
    private $afiFecnac;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $afiTipo;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $afiTipdoc;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="afi_nrodoc", type="integer", length=8)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $afiNrodoc;

    /**
     * @ORM\Column(type="integer")
     */
    private $afiMatricula;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $afiDireccion;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $afiLocalidad;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $afiProvincia;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $afiZona;

    /**
     * @ORM\Column(type="integer")
     */
    private $afiCodpos;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $afiTelefono1;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $afiTelefono2;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $afiMail;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $afiMailAlternativo;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $afiCivil;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $afiCuit;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $afiFicha;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $afiSexo;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="Familiar", mappedBy="afiliado", cascade={"all"}, orphanRemoval=true)
     */
    private $afiFamiliares;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="AfiliadosTitulos", mappedBy="afiliado", cascade={"all"}, orphanRemoval=true)
     */
    private $afiTitulos;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="Tarjeta", mappedBy="afiliado", cascade={"all"}, orphanRemoval=true)
     */
    private $afiTarjetas;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Beneficio")
     * @ORM\JoinTable(name="afiliados_beneficios",
     *      joinColumns={@ORM\JoinColumn(name="afi_nrodoc", referencedColumnName="afi_nrodoc")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="beneficio_id", referencedColumnName="id")}
     *      )
     */
    private $beneficios;

    public function __construct()
    {
        $this->estudios = new ArrayCollection();
        $this->afiFamiliares = new ArrayCollection();
        $this->afiTitulos = new ArrayCollection();
        $this->afiTarjetas = new ArrayCollection();
        $this->beneficios = new ArrayCollection();
    }

    public function jsonSerialize(): array {
        return [
            'afi_nrodoc' => $this->afiNrodoc,
            'afi_nombre' => $this->afiNombre,
            'afi_titulos'   => $this->afiTitulos,
            'afi_tarjetas'   => $this->afiTarjetas,
        ];
    }

    public function getId()
    {
        return $this->getAfiTipdoc().$this->getAfiNrodoc();
    }

    public function getAfiNombre(): ?string
    {
        return $this->afiNombre;
    }

    public function setAfiNombre(string $afiNombre): self
    {
        $this->afiNombre = $afiNombre;

        return $this;
    }

    public function getAfiFecnac(): ?\DateTimeInterface
    {
        return $this->afiFecnac;
    }

    public function setAfiFecnac(\DateTimeInterface $afiFecnac): self
    {
        $this->afiFecnac = $afiFecnac;

        return $this;
    }

    public function getAfiTipo(): ?string
    {
        return $this->afiTipo;
    }

    public function setAfiTipo(string $afiTipo): self
    {
        $this->afiTipo = $afiTipo;

        return $this;
    }

    public function getAfiTipdoc(): ?string
    {
        return $this->afiTipdoc;
    }

    public function setAfiTipdoc(string $afiTipdoc): self
    {
        $this->afiTipdoc = $afiTipdoc;

        return $this;
    }
    
    public function getAfiNrodoc(): ?int
    {
        return $this->afiNrodoc;
    }

    public function setAfiNrodoc(int $afiNrodoc): self
    {
        $this->afiNrodoc = $afiNrodoc;

        return $this;
    }

    public function getAfiMatricula(): ?int
    {
        return $this->afiMatricula;
    }

    public function setAfiMatricula(int $afiMatricula): self
    {
        $this->afiMatricula = $afiMatricula;

        return $this;
    }

    public function getAfiDireccion(): ?string
    {
        return $this->afiDireccion;
    }

    public function setAfiDireccion(string $afiDireccion): self
    {
        $this->afiDireccion = $afiDireccion;

        return $this;
    }

    public function getAfiLocalidad(): ?string
    {
        return $this->afiLocalidad;
    }

    public function setAfiLocalidad(string $afiLocalidad): self
    {
        $this->afiLocalidad = $afiLocalidad;

        return $this;
    }

    public function getAfiProvincia(): ?string
    {
        return $this->afiProvincia;
    }

    public function setAfiProvincia(string $afiProvincia): self
    {
        $this->afiProvincia = $afiProvincia;

        return $this;
    }

    public function getAfiZona(): ?string
    {
        return $this->afiZona;
    }

    public function setAfiZona(string $afiZona): self
    {
        $this->afiZona = $afiZona;

        return $this;
    }

    public function getAfiCodpos(): ?int
    {
        return $this->afiCodpos;
    }

    public function setAfiCodpos(int $afiCodpos): self
    {
        $this->afiCodpos = $afiCodpos;

        return $this;
    }

    public function getAfiTelefono1(): ?string
    {
        return $this->afiTelefono1;
    }

    public function setAfiTelefono1(string $afiTelefono1): self
    {
        $this->afiTelefono1 = $afiTelefono1;

        return $this;
    }

    public function getAfiTelefono2(): ?string
    {
        return $this->afiTelefono2;
    }

    public function setAfiTelefono2(string $afiTelefono2): self
    {
        $this->afiTelefono2 = $afiTelefono2;

        return $this;
    }

    public function getAfiMail(): ?string
    {
        return $this->afiMail;
    }

    public function setAfiMail(string $afiMail): self
    {
        $this->afiMail = $afiMail;

        return $this;
    }

    public function getAfiMailAlternativo(): ?string
    {
        return $this->afiMailAlternativo;
    }

    public function setAfiMailAlternativo(?string $afiMailAlternativo): self
    {
        $this->afiMailAlternativo = $afiMailAlternativo;

        return $this;
    }

    public function getAfiCivil(): ?string
    {
        return $this->afiCivil;
    }

    public function setAfiCivil(string $afiCivil): self
    {
        $this->afiCivil = $afiCivil;

        return $this;
    }

    public function getAfiCuit(): ?string
    {
        return $this->afiCuit;
    }

    public function setAfiCuit(string $afiCuit): self
    {
        $this->afiCuit = $afiCuit;

        return $this;
    }


    public function getAfiFicha(): ?string
    {
        return $this->afiFicha;
    }

    public function setAfiFicha(string $afiFicha): self
    {
        $this->afiFicha = $afiFicha;

        return $this;
    }

    public function getAfiSexo(): ?string
    {
        return $this->afiSexo;
    }

    public function setAfiSexo(string $afiSexo): self
    {
        $this->afiSexo = $afiSexo;

        return $this;
    }

    public function getAfiEstudioContable(): ?EstudioContable
    {
        return $this->afiEstudioContable;
    }

    public function setAfiEstudioContable(?EstudioContable $afiEstudioContable): self
    {
        $this->afiEstudioContable = $afiEstudioContable;

        return $this;
    }

    /**
     * Add afiFamiliares.
     *
     * @param \App\Entity\Familiar $afiFamiliares
     *
     * @return Familiar
     */
    public function addAfiFamiliar(\App\Entity\Familiar $afiFamiliar)
    {
        $this->afiFamiliares[] = $afiFamiliar;

        return $this;
    }

    /**
     * Remove afiFamiliares.
     *
     * @param \App\Entity\Familiar $afiFamiliares
     */
    public function removeAfiFamiliar(\App\Entity\Familiar $afiFamiliar)
    {
        $this->afiFamiliares->removeElement($afiFamiliar);
    }

    /**
     * Get afiFamiliares.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAfiFamiliares()
    {
        return $this->afiFamiliares;
    }

    /**
     * Add afiTitulos.
     *
     * @param \App\Entity\AfiliadosTitulos $afiTitulos
     *
     * @return AfiliadosTitulos
     */
    public function addAfiTitulo(\App\Entity\AfiliadosTitulos $afiTitulo)
    {
        $this->afiTitulos[] = $afiTitulo;

        return $this;
    }

    /**
     * Remove afiTitulos.
     *
     * @param \App\Entity\AfiliadosTitulos $afiTitulos
     */
    public function removeAfiTitulo(\App\Entity\AfiliadosTitulos $afiTitulo)
    {
        $this->afiTitulos->removeElement($afiTitulo);
    }

    /**
     * Get afiTitulos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAfiTitulos()
    {
        return $this->afiTitulos;
    }

    /**
     * @return Collection|Beneficio[]
     */
    public function getBeneficios(): Collection
    {
        return $this->beneficios;
    }

    public function addBeneficio(Beneficio $beneficio): self
    {
        if (!$this->beneficios->contains($beneficio)) {
            $this->beneficios[] = $beneficio;
        }

        return $this;
    }

    public function removeBeneficio(Beneficio $beneficio): self
    {
        if ($this->beneficios->contains($beneficio)) {
            $this->beneficios->removeElement($beneficio);
        }

        return $this;
    }

    /**
     * @return Collection|Tarjeta[]
     */
    public function getAfiTarjetas(): Collection
    {
        return $this->afiTarjetas;
    }

    public function addAfiTarjeta(Tarjeta $afiTarjeta): self
    {
        if (!$this->afiTarjetas->contains($afiTarjeta)) {
            $this->afiTarjetas[] = $afiTarjeta;
        }

        return $this;
    }

    public function removeAfiTarjeta(Tarjeta $afiTarjeta): self
    {
        if ($this->afiTarjetas->contains($afiTarjeta)) {
            $this->afiTarjetas->removeElement($afiTarjeta);
        }

        return $this;
    }

}
