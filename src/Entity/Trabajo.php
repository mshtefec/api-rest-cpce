<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trabajo
 *
 * @ORM\Table(name="trabajo")
 * @ORM\Entity
 */
class Trabajo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tra_nroasi", type="bigint", nullable=true, unique=true)
     */
    private $nroAsiento;

    /**
     * @var integer
     *
     * @ORM\Column(name="tra_nrolegali", type="integer", nullable=true, unique=true)
     */
    private $nroLegalizacion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id para el form filter
     *
     * @return integer 
     */
    public function setId($id)
    {
        return $this->id;
    }

    /**
     * Set nroAsiento
     *
     * @param integer $nroAsiento
     * @return Trabajo
     */
    public function setNroAsiento($nroAsiento)
    {
        $this->nroAsiento = $nroAsiento;

        return $this;
    }

    /**
     * Get nroAsiento
     *
     * @return integer 
     */
    public function getNroAsiento()
    {
        return $this->nroAsiento;
    }

    /**
     * Set nroLegalizacion
     *
     * @param integer $nroLegalizacion
     * @return Trabajo
     */
    public function setNroLegalizacion($nroLegalizacion)
    {
        $this->nroLegalizacion = $nroLegalizacion;

        return $this;
    }

    /**
     * Get nroLegalizacion
     *
     * @return integer 
     */
    public function getNroLegalizacion()
    {
        return $this->nroLegalizacion;
    }
}
