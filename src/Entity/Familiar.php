<?php

namespace App\Entity;

use App\Repository\FamiliarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FamiliarRepository::class)
 */
class Familiar implements \JsonSerializable
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Afiliado")
     * @ORM\JoinColumn(name="afiliado", referencedColumnName="afi_nrodoc")
     */
    private $afiliado;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $apellido;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(name="fecha_nacimiento", type="datetime")
     */
    private $fechaNacimiento;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $localidad;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $provincia;

    /**
     * @ORM\Column(name="codigo_postal", type="integer")
     */
    private $codigoPostal;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $telefono;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $parentesco;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $mail;

    /**
     * @ORM\Column(name="estado_civil", type="string", length=2)
     */
    private $estadoCivil;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $cuit;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $sexo;

    /**
     * @ORM\Column(type="string", length=22, nullable=true)
     */
    private $cbu;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Beneficio")
     * @ORM\JoinTable(name="familiar_beneficios",
     *      joinColumns={@ORM\JoinColumn(name="familiar_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="beneficio_id", referencedColumnName="id")}
     *      )
     */
    private $beneficios;

    public function __construct()
    {
        $this->beneficios = new ArrayCollection();
    }

    public function jsonSerialize(): array {
        return [
            'id' => $this->id,
            'apellido' => $this->apellido,
            'nombre' => $this->nombre,
            'dni' => $this->dni,
            'parentesco' => $this->parentesco,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getParentesco(): ?string
    {
        return $this->parentesco;
    }
  
    public function setParentesco(string $parentesco): ?self
    {
        $this->parentesco = $parentesco;

        return $this;
    }
    
    public function getAfiliado(): ?Afiliado
    {
        return $this->afiliado;
    }

    public function setAfiliado(?Afiliado $afiliado): self
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    public function getCuit(): ?string
    {
        return $this->cuit;
    }
  
    public function setCuit(string $cuit): ?self
    {
        $this->cuit = $cuit;

        return $this;
    }

    public function getDni(): ?int
    {
        return $this->dni;
    }
  
    public function setDni(int $dni): ?self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento(\DateTimeInterface $fechaNacimiento): self
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getCodigoPostal(): ?int
    {
        return $this->codigoPostal;
    }

    public function setCodigoPostal(int $codigoPostal): self
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getEstadoCivil(): ?string
    {
        return $this->estadoCivil;
    }

    public function setEstadoCivil(string $estadoCivil): self
    {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getCbu(): ?string
    {
        return $this->cbu;
    }

    public function setCbu(?string $cbu): self
    {
        $this->cbu = $cbu;

        return $this;
    }

    public function getAfiObrasocial(): ?int
    {
        return $this->afiObrasocial;
    }

    public function setAfiObrasocial(int $afiObrasocial): self
    {
        $this->afiObrasocial = $afiObrasocial;

        return $this;
    }

    /**
     * @return Collection|Beneficio[]
     */
    public function getBeneficios(): Collection
    {
        return $this->beneficios;
    }

    public function addBeneficio(Beneficio $beneficio): self
    {
        if (!$this->beneficios->contains($beneficio)) {
            $this->beneficios[] = $beneficio;
        }

        return $this;
    }

    public function removeBeneficio(Beneficio $beneficio): self
    {
        if ($this->beneficios->contains($beneficio)) {
            $this->beneficios->removeElement($beneficio);
        }

        return $this;
    }
    
}
