<?php

namespace App\Entity;

use App\Repository\TotalesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="totales", indexes={@ORM\Index(name="skmayor", columns={"tot_fecha", "tot_unegos"}), @ORM\Index(name="skcuenta", columns={"tot_tipdoc", "tot_nrodoc", "tot_fecha"}), @ORM\Index(name="skcaja", columns={"tot_nrocli", "tot_caja", "tot_zeta"}), @ORM\Index(name="skfecha", columns={"tot_fecha", "tot_unegos", "tot_nrocli"}), @ORM\Index(name="skproceso", columns={"tot_proceso", "tot_tipdoc", "tot_nrodoc", "tot_fecha"}), @ORM\Index(name="skcomproba", columns={"tot_unegos", "tot_proceso", "tot_nrocli", "tot_nrocom", "tot_item", "tot_nrocuo"}), @ORM\Index(name="skdocumento", columns={"tot_tipdoc", "tot_nrodoc", "tot_fecha"}), @ORM\Index(name="skcheques", columns={"tot_nrocheque"}), @ORM\Index(name="sklegalizacion", columns={"tot_nrolegali"})})
 * @ORM\Entity(repositoryClass=TotalesRepository::class)
 */
class Totales implements \JsonSerializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="tot_proceso", type="string", length=6, nullable=false)
     */
    private $totProceso;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nrocom", type="integer", nullable=false)
     */
    private $totNrocom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tot_fecha", type="datetime", nullable=false)
     */
    private $totFecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tot_fecven", type="date", nullable=false)
     */
    private $totFecven;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_forpag", type="string", length=8, nullable=false)
     */
    private $totForpag;

    /**
     * @var float
     *
     * @ORM\Column(name="tot_debe", type="float", precision=12, scale=2, nullable=false)
     */
    private $totDebe;

    /**
     * @var float
     *
     * @ORM\Column(name="tot_haber", type="float", precision=12, scale=2, nullable=false)
     */
    private $totHaber;

    /**
     * @ORM\ManyToOne(targetEntity="Plancuen")
     * @ORM\JoinColumn(name="tot_nropla", referencedColumnName="pla_nropla")
     */
    private $totNropla;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_subpla", type="string", length=20, nullable=false)
     */
    private $totSubpla;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_tipdoc", type="string", length=3, nullable=false)
     */
    private $totTipdoc;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nrodoc", type="integer", nullable=false)
     */
    private $totNrodoc;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_titulo", type="string", length=2, nullable=false)
     */
    private $totTitulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_matricula", type="integer", nullable=false)
     */
    private $totMatricula;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nroope", type="integer", nullable=false)
     */
    private $totNroope;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nroreg", type="integer", nullable=false)
     */
    private $totNroreg;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_caja", type="integer", nullable=false)
     */
    private $totCaja;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_zeta", type="integer", nullable=false)
     */
    private $totZeta;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nrolegali", type="integer", nullable=false)
     */
    private $totNrolegali;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_concepto", type="string", length=50, nullable=false)
     */
    private $totConcepto;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_lote", type="bigint", nullable=false)
     */
    private $totLote;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_asiant", type="bigint", nullable=false)
     */
    private $totAsiant;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_estado", type="string", length=1, nullable=false)
     */
    private $totEstado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tot_fecconcilia", type="datetime", nullable=false)
     */
    private $totFecconcilia;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_opeconcilia", type="integer", nullable=false)
     */
    private $totOpeconcilia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tot_fecvta", type="date", nullable=false)
     */
    private $totFecvta;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_asicancel", type="bigint", nullable=false)
     */
    private $totAsicancel;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_imppag", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $totImppag;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_codretencion", type="integer", nullable=false)
     */
    private $totCodretencion;

    /**
     * @var float
     *
     * @ORM\Column(name="tot_bonifica", type="float", precision=10, scale=2, nullable=false)
     */
    private $totBonifica;

    /**
     * @var float
     *
     * @ORM\Column(name="tot_porcentaje", type="float", precision=7, scale=2, nullable=false)
     */
    private $totPorcentaje;

    /**
     * @var float
     *
     * @ORM\Column(name="tot_sobre", type="float", precision=10, scale=2, nullable=false)
     */
    private $totSobre;

    /**
     * @var float
     *
     * @ORM\Column(name="tot_iva", type="float", precision=10, scale=2, nullable=false)
     */
    private $totIva;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nrochequera", type="integer", nullable=false)
     */
    private $totNrochequera;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_letcheque", type="string", length=1, nullable=false)
     */
    private $totLetcheque;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nrocheque", type="integer", nullable=false)
     */
    private $totNrocheque;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tot_fecche", type="date", nullable=false)
     */
    private $totFecche;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tot_fecdif", type="date", nullable=false)
     */
    private $totFecdif;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_tipdes", type="string", length=3, nullable=false)
     */
    private $totTipdes;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nrodes", type="integer", nullable=false)
     */
    private $totNrodes;

    /**
     * @var string
     *
     * @ORM\Column(name="tot_estche", type="string", length=1, nullable=false)
     */
    private $totEstche;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_asigrupal", type="bigint", nullable=false)
     */
    private $totAsigrupal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tot_fecalt", type="date", nullable=false)
     */
    private $totFecalt;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nroasi", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $totNroasi;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nrocli", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $totNrocli;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_unegos", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $totUnegos;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_item", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $totItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="tot_nrocuo", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $totNrocuo;


    public function __toString()
    {
        return ' ';
    }

    public function jsonSerialize(): array {
        return [
            'tot_unegos' => $this->getTotUnegos(),
            'tot_nrocli' => $this->getTotNrocli(),
            'tot_item' => $this->getTotItem(),
            'tot_nrocuo' => $this->getTotNrocuo(),
            'tot_nroasi' => $this->getTotNroasi(),
            'tot_nrocom' => $this->getTotNrocom(),
            'tot_proceso' => $this->getTotProceso(),
            'tot_nroope' => $this->getTotNroope(),
            'tot_tipdoc' => $this->getTotTipdoc(),
            'tot_nrodoc' => $this->getTotNrodoc(),
            'tot_titulo' => $this->getTotTitulo(),
            'tot_matricula' => $this->getTotMatricula(),
            'tot_zeta' => $this->getTotZeta(),
            'tot_debe' => $this->getTotDebe(),
            'tot_haber' => $this->getTotHaber(),
            'tot_fecha' => $this->getTotFecha(),
            'tot_fecalt' => $this->getTotFecalt(),
        ];
    }

    public function getTotProceso(): ?string
    {
        return $this->totProceso;
    }

    public function setTotProceso(string $totProceso): self
    {
        $this->totProceso = $totProceso;

        return $this;
    }

    public function getTotNrocom(): ?int
    {
        return $this->totNrocom;
    }

    public function setTotNrocom(int $totNrocom): self
    {
        $this->totNrocom = $totNrocom;

        return $this;
    }

    public function getTotFecha(): ?\DateTimeInterface
    {
        return $this->totFecha;
    }

    public function setTotFecha(\DateTimeInterface $totFecha): self
    {
        $this->totFecha = $totFecha;

        return $this;
    }

    public function getTotFecven(): ?\DateTimeInterface
    {
        return $this->totFecven;
    }

    public function setTotFecven(\DateTimeInterface $totFecven): self
    {
        $this->totFecven = $totFecven;

        return $this;
    }

    public function getTotForpag(): ?string
    {
        return $this->totForpag;
    }

    public function setTotForpag(string $totForpag): self
    {
        $this->totForpag = $totForpag;

        return $this;
    }

    public function getTotDebe(): ?float
    {
        return $this->totDebe;
    }

    public function setTotDebe(float $totDebe): self
    {
        $this->totDebe = $totDebe;

        return $this;
    }

    public function getTotHaber(): ?float
    {
        return $this->totHaber;
    }

    public function setTotHaber(float $totHaber): self
    {
        $this->totHaber = $totHaber;

        return $this;
    }

    /**
     * Set totNropla
     *
     * @param string $totNropla
     * @return Totales
     */
    public function setTotNropla($totNropla)
    {
        $this->totNropla = $totNropla;

        return $this;
    }

    /**
     * Get totNropla
     *
     * @return string 
     */
    public function getTotNropla()
    {
        return $this->totNropla;
    }

    public function getTotSubpla(): ?string
    {
        return $this->totSubpla;
    }

    public function setTotSubpla(string $totSubpla): self
    {
        $this->totSubpla = $totSubpla;

        return $this;
    }

    public function getTotTipdoc(): ?string
    {
        return $this->totTipdoc;
    }

    public function setTotTipdoc(string $totTipdoc): self
    {
        $this->totTipdoc = $totTipdoc;

        return $this;
    }

    public function getTotNrodoc(): ?int
    {
        return $this->totNrodoc;
    }

    public function setTotNrodoc(int $totNrodoc): self
    {
        $this->totNrodoc = $totNrodoc;

        return $this;
    }

    public function getTotTitulo(): ?string
    {
        return $this->totTitulo;
    }

    public function setTotTitulo(string $totTitulo): self
    {
        $this->totTitulo = $totTitulo;

        return $this;
    }

    public function getTotMatricula(): ?int
    {
        return $this->totMatricula;
    }

    public function setTotMatricula(int $totMatricula): self
    {
        $this->totMatricula = $totMatricula;

        return $this;
    }

    public function getTotNroope(): ?int
    {
        return $this->totNroope;
    }

    public function setTotNroope(int $totNroope): self
    {
        $this->totNroope = $totNroope;

        return $this;
    }

    public function getTotNroreg(): ?int
    {
        return $this->totNroreg;
    }

    public function setTotNroreg(int $totNroreg): self
    {
        $this->totNroreg = $totNroreg;

        return $this;
    }

    public function getTotCaja(): ?int
    {
        return $this->totCaja;
    }

    public function setTotCaja(int $totCaja): self
    {
        $this->totCaja = $totCaja;

        return $this;
    }

    public function getTotZeta(): ?int
    {
        return $this->totZeta;
    }

    public function setTotZeta(int $totZeta): self
    {
        $this->totZeta = $totZeta;

        return $this;
    }

    public function getTotNrolegali(): ?int
    {
        return $this->totNrolegali;
    }

    public function setTotNrolegali(int $totNrolegali): self
    {
        $this->totNrolegali = $totNrolegali;

        return $this;
    }

    public function getTotConcepto(): ?string
    {
        return $this->totConcepto;
    }

    public function setTotConcepto(string $totConcepto): self
    {
        $this->totConcepto = $totConcepto;

        return $this;
    }

    public function getTotLote(): ?string
    {
        return $this->totLote;
    }

    public function setTotLote(string $totLote): self
    {
        $this->totLote = $totLote;

        return $this;
    }

    public function getTotAsiant(): ?string
    {
        return $this->totAsiant;
    }

    public function setTotAsiant(string $totAsiant): self
    {
        $this->totAsiant = $totAsiant;

        return $this;
    }

    public function getTotEstado(): ?string
    {
        return $this->totEstado;
    }

    public function setTotEstado(string $totEstado): self
    {
        $this->totEstado = $totEstado;

        return $this;
    }

    public function getTotFecconcilia(): ?\DateTimeInterface
    {
        return $this->totFecconcilia;
    }

    public function setTotFecconcilia(\DateTimeInterface $totFecconcilia): self
    {
        $this->totFecconcilia = $totFecconcilia;

        return $this;
    }

    public function getTotOpeconcilia(): ?int
    {
        return $this->totOpeconcilia;
    }

    public function setTotOpeconcilia(int $totOpeconcilia): self
    {
        $this->totOpeconcilia = $totOpeconcilia;

        return $this;
    }

    public function getTotFecvta(): ?\DateTimeInterface
    {
        return $this->totFecvta;
    }

    public function setTotFecvta(\DateTimeInterface $totFecvta): self
    {
        $this->totFecvta = $totFecvta;

        return $this;
    }

    public function getTotAsicancel(): ?string
    {
        return $this->totAsicancel;
    }

    public function setTotAsicancel(string $totAsicancel): self
    {
        $this->totAsicancel = $totAsicancel;

        return $this;
    }

    public function getTotImppag(): ?string
    {
        return $this->totImppag;
    }

    public function setTotImppag(string $totImppag): self
    {
        $this->totImppag = $totImppag;

        return $this;
    }

    public function getTotCodretencion(): ?int
    {
        return $this->totCodretencion;
    }

    public function setTotCodretencion(int $totCodretencion): self
    {
        $this->totCodretencion = $totCodretencion;

        return $this;
    }

    public function getTotBonifica(): ?float
    {
        return $this->totBonifica;
    }

    public function setTotBonifica(float $totBonifica): self
    {
        $this->totBonifica = $totBonifica;

        return $this;
    }

    public function getTotPorcentaje(): ?float
    {
        return $this->totPorcentaje;
    }

    public function setTotPorcentaje(float $totPorcentaje): self
    {
        $this->totPorcentaje = $totPorcentaje;

        return $this;
    }

    public function getTotSobre(): ?float
    {
        return $this->totSobre;
    }

    public function setTotSobre(float $totSobre): self
    {
        $this->totSobre = $totSobre;

        return $this;
    }

    public function getTotIva(): ?float
    {
        return $this->totIva;
    }

    public function setTotIva(float $totIva): self
    {
        $this->totIva = $totIva;

        return $this;
    }

    public function getTotNrochequera(): ?int
    {
        return $this->totNrochequera;
    }

    public function setTotNrochequera(int $totNrochequera): self
    {
        $this->totNrochequera = $totNrochequera;

        return $this;
    }

    public function getTotLetcheque(): ?string
    {
        return $this->totLetcheque;
    }

    public function setTotLetcheque(string $totLetcheque): self
    {
        $this->totLetcheque = $totLetcheque;

        return $this;
    }

    public function getTotNrocheque(): ?int
    {
        return $this->totNrocheque;
    }

    public function setTotNrocheque(int $totNrocheque): self
    {
        $this->totNrocheque = $totNrocheque;

        return $this;
    }

    public function getTotFecche(): ?\DateTimeInterface
    {
        return $this->totFecche;
    }

    public function setTotFecche(\DateTimeInterface $totFecche): self
    {
        $this->totFecche = $totFecche;

        return $this;
    }

    public function getTotFecdif(): ?\DateTimeInterface
    {
        return $this->totFecdif;
    }

    public function setTotFecdif(\DateTimeInterface $totFecdif): self
    {
        $this->totFecdif = $totFecdif;

        return $this;
    }

    public function getTotTipdes(): ?string
    {
        return $this->totTipdes;
    }

    public function setTotTipdes(string $totTipdes): self
    {
        $this->totTipdes = $totTipdes;

        return $this;
    }

    public function getTotNrodes(): ?int
    {
        return $this->totNrodes;
    }

    public function setTotNrodes(int $totNrodes): self
    {
        $this->totNrodes = $totNrodes;

        return $this;
    }

    public function getTotEstche(): ?string
    {
        return $this->totEstche;
    }

    public function setTotEstche(string $totEstche): self
    {
        $this->totEstche = $totEstche;

        return $this;
    }

    public function getTotAsigrupal(): ?string
    {
        return $this->totAsigrupal;
    }

    public function setTotAsigrupal(string $totAsigrupal): self
    {
        $this->totAsigrupal = $totAsigrupal;

        return $this;
    }

    public function getTotFecalt(): ?\DateTimeInterface
    {
        return $this->totFecalt;
    }

    public function setTotFecalt(\DateTimeInterface $totFecalt): self
    {
        $this->totFecalt = $totFecalt;

        return $this;
    }

    public function getTotNroasi(): ?string
    {
        return $this->totNroasi;
    }

    public function getTotNrocli(): ?int
    {
        return $this->totNrocli;
    }

    public function getTotUnegos(): ?int
    {
        return $this->totUnegos;
    }

    public function getTotItem(): ?int
    {
        return $this->totItem;
    }

    public function getTotNrocuo(): ?int
    {
        return $this->totNrocuo;
    }
}
