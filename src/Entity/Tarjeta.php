<?php

namespace App\Entity;

use App\Repository\TarjetaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarjetaRepository::class)
 */
class Tarjeta implements \JsonSerializable {

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=16)
     */
    private $numero;

    /**
     * @ORM\ManyToOne(targetEntity="Afiliado")
     * @ORM\JoinColumn(name="afiliado", referencedColumnName="afi_nrodoc", nullable=FALSE)
     */
    private $afiliado;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $codigo_seguridad;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha_vencimiento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $banco;

    /**
     * @ORM\Column(type="integer")
     */
    private $activo;

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function getAfiliado(): ?Afiliado
    {
        return $this->afiliado;
    }

    public function setAfiliado(?Afiliado $afiliado): self
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getCodigoSeguridad(): ?string
    {
        return $this->codigo_seguridad;
    }

    public function setCodigoSeguridad(string $codigo_seguridad): self
    {
        $this->codigo_seguridad = $codigo_seguridad;

        return $this;
    }

    public function getFechaVencimiento(): ?\DateTimeInterface
    {
        return $this->fecha_vencimiento;
    }

    public function setFechaVencimiento(\DateTimeInterface $fecha_vencimiento): self
    {
        $this->fecha_vencimiento = $fecha_vencimiento;

        return $this;
    }

    public function getBanco(): ?string
    {
        return $this->banco;
    }

    public function setBanco(string $banco): self
    {
        $this->banco = $banco;

        return $this;
    }

    public function getActivo(): ?int
    {
        return $this->activo;
    }

    public function setActivo(int $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    public function jsonSerialize(): array {
        return [
            'numero' => $this->numero,
            'afiliado' => $this->afiliado,
            'nombre' => $this->nombre,
            'fecha_vencimiento' => $this->fecha_vencimiento,
            'tipo' => $this->tipo,
            'banco' => $this->banco
        ];
    }
}