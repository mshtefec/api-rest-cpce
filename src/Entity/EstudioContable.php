<?php

namespace App\Entity;

use App\Repository\EstudioContableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EstudioContableRepository::class)
 * @ORM\Entity
 * @ORM\Table(name="estudio_contable")
 */

class EstudioContable implements \JsonSerializable
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $denominacion;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $localidad;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $provincia;

    /**
     * @ORM\Column(name="codigo_postal", type="integer")
     */
    private $codigoPostal;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $zona;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $cuit;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Afiliado")
     * @ORM\JoinTable(name="estudiocontables_afiliados",
     *      joinColumns={@ORM\JoinColumn(name="estudio_contable", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="afi_nrodoc", referencedColumnName="afi_nrodoc")}
     *      )
     */
    private $integrantes;

    public function __construct()
    { 
        $this->integrantes = new ArrayCollection();
    }

    public function jsonSerialize(): array {
        return [
            'id' => $this->id,
            'denominacion' => $this->denominacion,
            'integrantes' => $this->integrantes
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getZona(): ?string
    {
        return $this->zona;
    }
  
    public function setZona(string $zona): ?self
    {
        $this->zona = $zona;

        return $this;
    }

    public function getCuit(): ?int
    {
        return $this->cuit;
    }
  
    public function setCuit(int $cuit): ?self
    {
        $this->cuit = $cuit;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getDenominacion(): ?string
    {
        return $this->denominacion;
    }

    public function setDenominacion(string $denominacion): self
    {
        $this->denominacion = $denominacion;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getCodigoPostal(): ?int
    {
        return $this->codigoPostal;
    }

    public function setCodigoPostal(int $codigoPostal): self
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Afiliado[]
     */
    public function getIntegrantes(): Collection
    {
        return $this->integrantes;
    }

    public function addIntegrante(Afiliado $integrante): self
    {
        if (!$this->integrantes->contains($integrante)) {
            $this->integrantes[] = $integrante;
        }

        return $this;
    }

    public function removeIntegrante(Afiliado $integrante): self
    {
        if ($this->integrantes->contains($integrante)) {
            $this->integrantes->removeElement($integrante);
        }

        return $this;
    }
    

}
