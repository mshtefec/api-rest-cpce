<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cheques
 *
 * @ORM\Table(name="cheques")
 * @ORM\Entity
 */
class Cheques
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FECHA", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var integer
     *
     * @ORM\Column(name="COMPROB", type="integer", nullable=true)
     */
    private $comprob;

    /**
     * @var string
     *
     * @ORM\Column(name="PROCESO", type="string", length=6, nullable=true)
     */
    private $proceso;

    /**
     * @var string
     *
     * @ORM\Column(name="CHEQUE", type="string", length=12, nullable=true)
     */
    private $cheque;

    /**
     * @var string
     *
     * @ORM\Column(name="BANCO", type="string", length=20, nullable=true)
     */
    private $banco;

    /**
     * @var string
     *
     * @ORM\Column(name="SUCURSAL", type="string", length=15, nullable=true)
     */
    private $sucursal;

    /**
     * @var string
     *
     * @ORM\Column(name="CUENTA", type="string", length=10, nullable=true)
     */
    private $cuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="PLAZO", type="string", length=2, nullable=true)
     */
    private $plazo;

    /**
     * @var float
     *
     * @ORM\Column(name="IMPORTE", type="float", precision=24, scale=2, nullable=true)
     */
    private $importe;

    /**
     * @var boolean
     *
     * @ORM\Column(name="COLUMNA", type="boolean", nullable=false)
     */
    private $columna;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EFECTIVIZA", type="datetime", nullable=true)
     */
    private $efectiviza;

    /**
     * @var string
     *
     * @ORM\Column(name="ESTADO", type="string", length=1, nullable=true)
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FPDCHEQ", type="datetime", nullable=true)
     */
    private $fpdcheq;

    /**
     * @var string
     *
     * @ORM\Column(name="MATRICULA", type="string", length=5, nullable=true)
     */
    private $matricula;

    /**
     * @var string
     *
     * @ORM\Column(name="ORIGEN", type="string", length=1, nullable=true)
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="INSTIT", type="string", length=2, nullable=true)
     */
    private $instit;

    /**
     * @var float
     *
     * @ORM\Column(name="ASIENTO", type="float", precision=53, scale=0, nullable=true)
     */
    private $asiento;

    /**
     * @var string
     *
     * @ORM\Column(name="ZONA", type="string", length=4, nullable=true)
     */
    private $zona;

    /**
     * @var string
     *
     * @ORM\Column(name="CAMPO1", type="string", length=1, nullable=true)
     */
    private $campo1;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getComprob(): ?int
    {
        return $this->comprob;
    }

    public function setComprob(?int $comprob): self
    {
        $this->comprob = $comprob;

        return $this;
    }

    public function getProceso(): ?string
    {
        return $this->proceso;
    }

    public function setProceso(?string $proceso): self
    {
        $this->proceso = $proceso;

        return $this;
    }

    public function getCheque(): ?string
    {
        return $this->cheque;
    }

    public function setCheque(?string $cheque): self
    {
        $this->cheque = $cheque;

        return $this;
    }

    public function getBanco(): ?string
    {
        return $this->banco;
    }

    public function setBanco(?string $banco): self
    {
        $this->banco = $banco;

        return $this;
    }

    public function getSucursal(): ?string
    {
        return $this->sucursal;
    }

    public function setSucursal(?string $sucursal): self
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    public function getCuenta(): ?string
    {
        return $this->cuenta;
    }

    public function setCuenta(?string $cuenta): self
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    public function getPlazo(): ?string
    {
        return $this->plazo;
    }

    public function setPlazo(?string $plazo): self
    {
        $this->plazo = $plazo;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(?float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getColumna(): ?bool
    {
        return $this->columna;
    }

    public function setColumna(bool $columna): self
    {
        $this->columna = $columna;

        return $this;
    }

    public function getEfectiviza(): ?\DateTimeInterface
    {
        return $this->efectiviza;
    }

    public function setEfectiviza(?\DateTimeInterface $efectiviza): self
    {
        $this->efectiviza = $efectiviza;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFpdcheq(): ?\DateTimeInterface
    {
        return $this->fpdcheq;
    }

    public function setFpdcheq(?\DateTimeInterface $fpdcheq): self
    {
        $this->fpdcheq = $fpdcheq;

        return $this;
    }

    public function getMatricula(): ?string
    {
        return $this->matricula;
    }

    public function setMatricula(?string $matricula): self
    {
        $this->matricula = $matricula;

        return $this;
    }

    public function getOrigen(): ?string
    {
        return $this->origen;
    }

    public function setOrigen(?string $origen): self
    {
        $this->origen = $origen;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getInstit(): ?string
    {
        return $this->instit;
    }

    public function setInstit(?string $instit): self
    {
        $this->instit = $instit;

        return $this;
    }

    public function getAsiento(): ?float
    {
        return $this->asiento;
    }

    public function setAsiento(?float $asiento): self
    {
        $this->asiento = $asiento;

        return $this;
    }

    public function getZona(): ?string
    {
        return $this->zona;
    }

    public function setZona(?string $zona): self
    {
        $this->zona = $zona;

        return $this;
    }

    public function getCampo1(): ?string
    {
        return $this->campo1;
    }

    public function setCampo1(?string $campo1): self
    {
        $this->campo1 = $campo1;

        return $this;
    }

}
