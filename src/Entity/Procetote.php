<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Procetote
 *
 * @ORM\Table(name="procetote")
 * @ORM\Entity
 */
class Procetote
{
    /**
     * @var string
     *
     * @ORM\Column(name="pto_nropla", type="string", length=9, nullable=false)
     */
    private $ptoNropla;

    /**
     * @var string
     *
     * @ORM\Column(name="pto_tipmov", type="string", length=1, nullable=false)
     */
    private $ptoTipmov;

    /**
     * @var float
     *
     * @ORM\Column(name="pto_debe", type="float", precision=12, scale=2, nullable=false)
     */
    private $ptoDebe;

    /**
     * @var float
     *
     * @ORM\Column(name="pto_haber", type="float", precision=12, scale=2, nullable=false)
     */
    private $ptoHaber;

    /**
     * @var string
     *
     * @ORM\Column(name="pto_tabla", type="string", length=15, nullable=false)
     */
    private $ptoTabla;

    /**
     * @var string
     *
     * @ORM\Column(name="pto_campo", type="string", length=20, nullable=false)
     */
    private $ptoCampo;

    /**
     * @var string
     *
     * @ORM\Column(name="pto_tipcam", type="string", length=1, nullable=false)
     */
    private $ptoTipcam;

    /**
     * @var float
     *
     * @ORM\Column(name="pto_valor", type="float", precision=12, scale=2, nullable=false)
     */
    private $ptoValor;

    /**
     * @var string
     *
     * @ORM\Column(name="pto_formula", type="string", length=120, nullable=false)
     */
    private $ptoFormula;

    /**
     * @var integer
     *
     * @ORM\Column(name="pto_lote", type="bigint", nullable=false)
     */
    private $ptoLote;

    /**
     * @var string
     *
     * @ORM\Column(name="pto_codpro", type="string", length=6)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $ptoCodpro;

    /**
     * @var integer
     *
     * @ORM\Column(name="pto_item", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $ptoItem;

    public function getPtoNropla(): ?string
    {
        return $this->ptoNropla;
    }

    public function setPtoNropla(string $ptoNropla): self
    {
        $this->ptoNropla = $ptoNropla;

        return $this;
    }

    public function getPtoTipmov(): ?string
    {
        return $this->ptoTipmov;
    }

    public function setPtoTipmov(string $ptoTipmov): self
    {
        $this->ptoTipmov = $ptoTipmov;

        return $this;
    }

    public function getPtoDebe(): ?float
    {
        return $this->ptoDebe;
    }

    public function setPtoDebe(float $ptoDebe): self
    {
        $this->ptoDebe = $ptoDebe;

        return $this;
    }

    public function getPtoHaber(): ?float
    {
        return $this->ptoHaber;
    }

    public function setPtoHaber(float $ptoHaber): self
    {
        $this->ptoHaber = $ptoHaber;

        return $this;
    }

    public function getPtoTabla(): ?string
    {
        return $this->ptoTabla;
    }

    public function setPtoTabla(string $ptoTabla): self
    {
        $this->ptoTabla = $ptoTabla;

        return $this;
    }

    public function getPtoCampo(): ?string
    {
        return $this->ptoCampo;
    }

    public function setPtoCampo(string $ptoCampo): self
    {
        $this->ptoCampo = $ptoCampo;

        return $this;
    }

    public function getPtoTipcam(): ?string
    {
        return $this->ptoTipcam;
    }

    public function setPtoTipcam(string $ptoTipcam): self
    {
        $this->ptoTipcam = $ptoTipcam;

        return $this;
    }

    public function getPtoValor(): ?float
    {
        return $this->ptoValor;
    }

    public function setPtoValor(float $ptoValor): self
    {
        $this->ptoValor = $ptoValor;

        return $this;
    }

    public function getPtoFormula(): ?string
    {
        return $this->ptoFormula;
    }

    public function setPtoFormula(string $ptoFormula): self
    {
        $this->ptoFormula = $ptoFormula;

        return $this;
    }

    public function getPtoLote(): ?string
    {
        return $this->ptoLote;
    }

    public function setPtoLote(string $ptoLote): self
    {
        $this->ptoLote = $ptoLote;

        return $this;
    }

    public function getPtoCodpro(): ?string
    {
        return $this->ptoCodpro;
    }

    public function getPtoItem(): ?int
    {
        return $this->ptoItem;
    }



    
}
