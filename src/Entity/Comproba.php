<?php

namespace App\Entity;

use App\Repository\ComprobaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comproba
 *
 * @ORM\Table(name="comproba", indexes={@ORM\Index(name="SKLOTE", columns={"com_lote"}), @ORM\Index(name="SKPREIMPRESO", columns={"com_preimpreso"}), @ORM\Index(name="SKFECHA", columns={"com_fecha", "com_unegos", "com_nrocli"}), @ORM\Index(name="SKCOMPROBA", columns={"com_unegos", "com_proceso", "com_nrocli", "com_nrocom"}), @ORM\Index(name="SKGRUPAL", columns={"com_asigrupal"})})
 * @ORM\Entity
 */
class Comproba
{
    /**
     * @var integer
     *
     * @ORM\Column(name="com_unegos", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $comUnegos;

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nrocli", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $comNrocli;

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nroasi", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $comNroasi;

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nrodeleg", type="integer", nullable=false)
     */
    private $comNrodeleg = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_proceso", type="string", length=6, nullable=false)
     */
    private $comProceso = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nrocom", type="integer", nullable=false)
     */
    private $comNrocom = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_preimpreso", type="string", length=13, nullable=false)
     */
    private $comPreimpreso = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nroope", type="integer", nullable=false)
     */
    private $comNroope = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="com_fecha", type="datetime", nullable=false)
     */
    private $comFecha;

    /**
     * @var string
     *
     * @ORM\Column(name="com_titulo", type="string", length=2, nullable=false)
     */
    private $comTitulo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_matricula", type="integer", nullable=false)
     */
    private $comMatricula = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_subcuenta", type="string", length=20, nullable=false)
     */
    private $comSubcuenta = '';

    /**
     * @var string
     *
     * @ORM\Column(name="com_tipdoc", type="string", length=3, nullable=false)
     */
    private $comTipdoc = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nrodoc", type="integer", nullable=false)
     */
    private $comNrodoc = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_tipcmt", type="string", length=3, nullable=false)
     */
    private $comTipcmt = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nrocmt", type="integer", nullable=false)
     */
    private $comNrocmt = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nrocuo", type="integer", nullable=false)
     */
    private $comNrocuo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nropre", type="integer", nullable=false)
     */
    private $comNropre = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_tipo", type="string", length=1, nullable=false)
     */
    private $comTipo = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_lote", type="bigint", nullable=false)
     */
    private $comLote = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_caja", type="integer", nullable=false)
     */
    private $comCaja = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nroreg", type="integer", nullable=false)
     */
    private $comNroreg = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_zeta", type="integer", nullable=false)
     */
    private $comZeta = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="com_total", type="float", precision=14, scale=2, nullable=false)
     */
    private $comTotal = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="com_tipanu", type="string", length=2, nullable=false)
     */
    private $comTipanu = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_punanu", type="integer", nullable=false)
     */
    private $comPunanu = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nroanu", type="integer", nullable=false)
     */
    private $comNroanu = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="com_fecalt", type="date", nullable=false)
     */
    private $comFecalt;

    /**
     * @var string
     *
     * @ORM\Column(name="com_estado", type="string", length=1, nullable=false)
     */
    private $comEstado = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="com_fecven", type="date", nullable=false)
     */
    private $comFecven;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="com_fecpag", type="date", nullable=false)
     */
    private $comFecpag;

    /**
     * @var float
     *
     * @ORM\Column(name="com_imppag", type="float", precision=14, scale=2, nullable=false)
     */
    private $comImppag = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="com_tipmov", type="string", length=1, nullable=false)
     */
    private $comTipmov = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_coeficiente", type="integer", nullable=false)
     */
    private $comCoeficiente = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_concepto1", type="string", length=100, nullable=false)
     */
    private $comConcepto1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="com_concepto2", type="string", length=100, nullable=false)
     */
    private $comConcepto2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="com_concepto3", type="string", length=100, nullable=false)
     */
    private $comConcepto3 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="com_leyenda", type="string", length=100, nullable=false)
     */
    private $comLeyenda = '';

    /**
     * @var string
     *
     * @ORM\Column(name="com_refano", type="string", length=4, nullable=false)
     */
    private $comRefano = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_refmes", type="string", length=2, nullable=false)
     */
    private $comRefmes = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_asiant", type="bigint", nullable=false)
     */
    private $comAsiant = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_tarea", type="integer", nullable=false)
     */
    private $comTarea = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_nrotrabajo", type="string", length=20, nullable=false)
     */
    private $comNrotrabajo = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_asigrupal", type="bigint", nullable=false)
     */
    private $comAsigrupal = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="com_nrolegali", type="integer", nullable=false)
     */
    private $comNrolegali = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="com_destinatario", type="string", length=255, nullable=true)
     */
    private $comDestinatario;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comFecpag = new \Datetime("0000-00-00 00:00:00");
        $this->comFecven = new \Datetime("0000-00-00 00:00:00");
    }

    public function getComUnegos(): ?int
    {
        return $this->comUnegos;
    }

    public function getComNrocli(): ?int
    {
        return $this->comNrocli;
    }

    public function getComNroasi(): ?string
    {
        return $this->comNroasi;
    }

    public function getComNrodeleg(): ?int
    {
        return $this->comNrodeleg;
    }

    public function setComNrodeleg(int $comNrodeleg): self
    {
        $this->comNrodeleg = $comNrodeleg;

        return $this;
    }

    public function getComProceso(): ?string
    {
        return $this->comProceso;
    }

    public function setComProceso(string $comProceso): self
    {
        $this->comProceso = $comProceso;

        return $this;
    }

    public function getComNrocom(): ?int
    {
        return $this->comNrocom;
    }

    public function setComNrocom(int $comNrocom): self
    {
        $this->comNrocom = $comNrocom;

        return $this;
    }

    public function getComPreimpreso(): ?string
    {
        return $this->comPreimpreso;
    }

    public function setComPreimpreso(string $comPreimpreso): self
    {
        $this->comPreimpreso = $comPreimpreso;

        return $this;
    }

    public function getComNroope(): ?int
    {
        return $this->comNroope;
    }

    public function setComNroope(int $comNroope): self
    {
        $this->comNroope = $comNroope;

        return $this;
    }

    public function getComFecha(): ?\DateTimeInterface
    {
        return $this->comFecha;
    }

    public function setComFecha(\DateTimeInterface $comFecha): self
    {
        $this->comFecha = $comFecha;

        return $this;
    }

    public function getComTitulo(): ?string
    {
        return $this->comTitulo;
    }

    public function setComTitulo(string $comTitulo): self
    {
        $this->comTitulo = $comTitulo;

        return $this;
    }

    public function getComMatricula(): ?int
    {
        return $this->comMatricula;
    }

    public function setComMatricula(int $comMatricula): self
    {
        $this->comMatricula = $comMatricula;

        return $this;
    }

    public function getComSubcuenta(): ?string
    {
        return $this->comSubcuenta;
    }

    public function setComSubcuenta(string $comSubcuenta): self
    {
        $this->comSubcuenta = $comSubcuenta;

        return $this;
    }

    public function getComTipdoc(): ?string
    {
        return $this->comTipdoc;
    }

    public function setComTipdoc(string $comTipdoc): self
    {
        $this->comTipdoc = $comTipdoc;

        return $this;
    }

    public function getComNrodoc(): ?int
    {
        return $this->comNrodoc;
    }

    public function setComNrodoc(int $comNrodoc): self
    {
        $this->comNrodoc = $comNrodoc;

        return $this;
    }

    public function getComTipcmt(): ?string
    {
        return $this->comTipcmt;
    }

    public function setComTipcmt(string $comTipcmt): self
    {
        $this->comTipcmt = $comTipcmt;

        return $this;
    }

    public function getComNrocmt(): ?int
    {
        return $this->comNrocmt;
    }

    public function setComNrocmt(int $comNrocmt): self
    {
        $this->comNrocmt = $comNrocmt;

        return $this;
    }

    public function getComNrocuo(): ?int
    {
        return $this->comNrocuo;
    }

    public function setComNrocuo(int $comNrocuo): self
    {
        $this->comNrocuo = $comNrocuo;

        return $this;
    }

    public function getComNropre(): ?int
    {
        return $this->comNropre;
    }

    public function setComNropre(int $comNropre): self
    {
        $this->comNropre = $comNropre;

        return $this;
    }

    public function getComTipo(): ?string
    {
        return $this->comTipo;
    }

    public function setComTipo(string $comTipo): self
    {
        $this->comTipo = $comTipo;

        return $this;
    }

    public function getComLote(): ?string
    {
        return $this->comLote;
    }

    public function setComLote(string $comLote): self
    {
        $this->comLote = $comLote;

        return $this;
    }

    public function getComCaja(): ?int
    {
        return $this->comCaja;
    }

    public function setComCaja(int $comCaja): self
    {
        $this->comCaja = $comCaja;

        return $this;
    }

    public function getComNroreg(): ?int
    {
        return $this->comNroreg;
    }

    public function setComNroreg(int $comNroreg): self
    {
        $this->comNroreg = $comNroreg;

        return $this;
    }

    public function getComZeta(): ?int
    {
        return $this->comZeta;
    }

    public function setComZeta(int $comZeta): self
    {
        $this->comZeta = $comZeta;

        return $this;
    }

    public function getComTotal(): ?float
    {
        return $this->comTotal;
    }

    public function setComTotal(float $comTotal): self
    {
        $this->comTotal = $comTotal;

        return $this;
    }

    public function getComTipanu(): ?string
    {
        return $this->comTipanu;
    }

    public function setComTipanu(string $comTipanu): self
    {
        $this->comTipanu = $comTipanu;

        return $this;
    }

    public function getComPunanu(): ?int
    {
        return $this->comPunanu;
    }

    public function setComPunanu(int $comPunanu): self
    {
        $this->comPunanu = $comPunanu;

        return $this;
    }

    public function getComNroanu(): ?int
    {
        return $this->comNroanu;
    }

    public function setComNroanu(int $comNroanu): self
    {
        $this->comNroanu = $comNroanu;

        return $this;
    }

    public function getComFecalt(): ?\DateTimeInterface
    {
        return $this->comFecalt;
    }

    public function setComFecalt(\DateTimeInterface $comFecalt): self
    {
        $this->comFecalt = $comFecalt;

        return $this;
    }

    public function getComEstado(): ?string
    {
        return $this->comEstado;
    }

    public function setComEstado(string $comEstado): self
    {
        $this->comEstado = $comEstado;

        return $this;
    }

    public function getComFecven(): ?\DateTimeInterface
    {
        return $this->comFecven;
    }

    public function setComFecven(\DateTimeInterface $comFecven): self
    {
        $this->comFecven = $comFecven;

        return $this;
    }

    public function getComFecpag(): ?\DateTimeInterface
    {
        return $this->comFecpag;
    }

    public function setComFecpag(\DateTimeInterface $comFecpag): self
    {
        $this->comFecpag = $comFecpag;

        return $this;
    }

    public function getComImppag(): ?float
    {
        return $this->comImppag;
    }

    public function setComImppag(float $comImppag): self
    {
        $this->comImppag = $comImppag;

        return $this;
    }

    public function getComTipmov(): ?string
    {
        return $this->comTipmov;
    }

    public function setComTipmov(string $comTipmov): self
    {
        $this->comTipmov = $comTipmov;

        return $this;
    }

    public function getComCoeficiente(): ?int
    {
        return $this->comCoeficiente;
    }

    public function setComCoeficiente(int $comCoeficiente): self
    {
        $this->comCoeficiente = $comCoeficiente;

        return $this;
    }

    public function getComConcepto1(): ?string
    {
        return $this->comConcepto1;
    }

    public function setComConcepto1(string $comConcepto1): self
    {
        $this->comConcepto1 = $comConcepto1;

        return $this;
    }

    public function getComConcepto2(): ?string
    {
        return $this->comConcepto2;
    }

    public function setComConcepto2(string $comConcepto2): self
    {
        $this->comConcepto2 = $comConcepto2;

        return $this;
    }

    public function getComConcepto3(): ?string
    {
        return $this->comConcepto3;
    }

    public function setComConcepto3(string $comConcepto3): self
    {
        $this->comConcepto3 = $comConcepto3;

        return $this;
    }

    public function getComLeyenda(): ?string
    {
        return $this->comLeyenda;
    }

    public function setComLeyenda(string $comLeyenda): self
    {
        $this->comLeyenda = $comLeyenda;

        return $this;
    }

    public function getComRefano(): ?string
    {
        return $this->comRefano;
    }

    public function setComRefano(string $comRefano): self
    {
        $this->comRefano = $comRefano;

        return $this;
    }

    public function getComRefmes(): ?string
    {
        return $this->comRefmes;
    }

    public function setComRefmes(string $comRefmes): self
    {
        $this->comRefmes = $comRefmes;

        return $this;
    }

    public function getComAsiant(): ?string
    {
        return $this->comAsiant;
    }

    public function setComAsiant(string $comAsiant): self
    {
        $this->comAsiant = $comAsiant;

        return $this;
    }

    public function getComTarea(): ?int
    {
        return $this->comTarea;
    }

    public function setComTarea(int $comTarea): self
    {
        $this->comTarea = $comTarea;

        return $this;
    }

    public function getComNrotrabajo(): ?string
    {
        return $this->comNrotrabajo;
    }

    public function setComNrotrabajo(string $comNrotrabajo): self
    {
        $this->comNrotrabajo = $comNrotrabajo;

        return $this;
    }

    public function getComAsigrupal(): ?string
    {
        return $this->comAsigrupal;
    }

    public function setComAsigrupal(string $comAsigrupal): self
    {
        $this->comAsigrupal = $comAsigrupal;

        return $this;
    }

    public function getComNrolegali(): ?int
    {
        return $this->comNrolegali;
    }

    public function setComNrolegali(int $comNrolegali): self
    {
        $this->comNrolegali = $comNrolegali;

        return $this;
    }

    public function getComDestinatario(): ?string
    {
        return $this->comDestinatario;
    }

    public function setComDestinatario(?string $comDestinatario): self
    {
        $this->comDestinatario = $comDestinatario;

        return $this;
    }
}
