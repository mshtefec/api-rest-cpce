<?php

namespace App\Entity;

use App\Repository\AdhesionesDebitoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdhesionesDebitoRepository::class)
 * @ORM\Entity
 * @ORM\Table(name="adhesiones_debito")
 */
class AdhesionesDebito implements \JsonSerializable {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Afiliado")
     * @ORM\JoinColumn(name="afiliado", referencedColumnName="afi_nrodoc", nullable=FALSE)
     */
    private $afiliado;

    /**
     * @ORM\ManyToOne(targetEntity="Tarjeta")
     * @ORM\JoinColumn(name="tarjeta", referencedColumnName="numero", nullable=FALSE)
     */
    private $tarjeta;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $tipo;

    /**
     * @ORM\Column(type="float", precision=14, scale=2)
     */
    private $importe;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="Plancuen")
     * @ORM\JoinColumn(name="cuenta", referencedColumnName="pla_nropla", nullable=FALSE)
     */
    private $cuenta;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $meses;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $estado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getCuenta(): ?Plancuen
    {
        return $this->cuenta;
    }

    public function setCuenta(?Plancuen $cuenta): self
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    public function getMeses(): ?string
    {
        return $this->meses;
    }

    public function setMeses(string $meses): self
    {
        $this->meses = $meses;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getAfiliado(): ?Afiliado
    {
        return $this->afiliado;
    }

    public function setAfiliado(?Afiliado $afiliado): self
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    public function getTarjeta(): ?Tarjeta
    {
        return $this->tarjeta;
    }

    public function setTarjeta(?Tarjeta $tarjeta): self
    {
        $this->tarjeta = $tarjeta;

        return $this;
    }

    public function jsonSerialize(): array {
        return [
            'id' => $this->id,
            'afiliado' => $this->afiliado,
            'tarjeta' => $this->tarjeta,
            'cuenta' => $this->cuenta,
            'estado' => $this->estado
        ];
    }
}