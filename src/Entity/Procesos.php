<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Procesos
 *
 * @ORM\Table(name="procesos")
 * @ORM\Entity
 */
class Procesos
{
    /**
     * @var string
     *
     * @ORM\Column(name="pro_nombre", type="string", length=60, nullable=false)
     */
    private $proNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_impresion", type="string", length=50, nullable=false)
     */
    private $proImpresion;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_operacion", type="string", length=30, nullable=false)
     */
    private $proOperacion;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_numerador", type="string", length=8, nullable=false)
     */
    private $proNumerador;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_leyenda1", type="string", length=30, nullable=false)
     */
    private $proLeyenda1;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_leyenda2", type="string", length=30, nullable=false)
     */
    private $proLeyenda2;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_leyenda3", type="string", length=60, nullable=false)
     */
    private $proLeyenda3;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_leyenda4", type="string", length=60, nullable=false)
     */
    private $proLeyenda4;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_cpto1", type="string", length=70, nullable=false)
     */
    private $proCpto1;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_cpto2", type="string", length=70, nullable=false)
     */
    private $proCpto2;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_cpto3", type="string", length=70, nullable=false)
     */
    private $proCpto3;

    /**
     * @var integer
     *
     * @ORM\Column(name="pro_copias", type="integer", nullable=false)
     */
    private $proCopias;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_buscaf", type="string", length=2, nullable=false)
     */
    private $proBuscaf;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_cajachica", type="string", length=2, nullable=false)
     */
    private $proCajachica;

    /**
     * @var integer
     *
     * @ORM\Column(name="pro_instit", type="integer", nullable=false)
     */
    private $proInstit;

    /**
     * @var integer
     *
     * @ORM\Column(name="pro_copias2", type="integer", nullable=false)
     */
    private $proCopias2;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_procancela", type="string", length=6, nullable=false)
     */
    private $proProcancela;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_bloqfech", type="string", length=2, nullable=false)
     */
    private $proBloqfech;

    /**
     * @var string
     *
     * @ORM\Column(name="LEYENDA", type="string", length=20, nullable=true)
     */
    private $leyenda;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_catexcl", type="string", length=70, nullable=true)
     */
    private $proCatexcl;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_reporte", type="string", length=50, nullable=false)
     */
    private $proReporte;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_destinatario", type="string", length=2, nullable=false)
     */
    private $proDestinatario;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_codigo", type="string", length=6)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $proCodigo;

    public function getProNombre(): ?string
    {
        return $this->proNombre;
    }

    public function setProNombre(string $proNombre): self
    {
        $this->proNombre = $proNombre;

        return $this;
    }

    public function getProImpresion(): ?string
    {
        return $this->proImpresion;
    }

    public function setProImpresion(string $proImpresion): self
    {
        $this->proImpresion = $proImpresion;

        return $this;
    }

    public function getProOperacion(): ?string
    {
        return $this->proOperacion;
    }

    public function setProOperacion(string $proOperacion): self
    {
        $this->proOperacion = $proOperacion;

        return $this;
    }

    public function getProNumerador(): ?string
    {
        return $this->proNumerador;
    }

    public function setProNumerador(string $proNumerador): self
    {
        $this->proNumerador = $proNumerador;

        return $this;
    }

    public function getProLeyenda1(): ?string
    {
        return $this->proLeyenda1;
    }

    public function setProLeyenda1(string $proLeyenda1): self
    {
        $this->proLeyenda1 = $proLeyenda1;

        return $this;
    }

    public function getProLeyenda2(): ?string
    {
        return $this->proLeyenda2;
    }

    public function setProLeyenda2(string $proLeyenda2): self
    {
        $this->proLeyenda2 = $proLeyenda2;

        return $this;
    }

    public function getProLeyenda3(): ?string
    {
        return $this->proLeyenda3;
    }

    public function setProLeyenda3(string $proLeyenda3): self
    {
        $this->proLeyenda3 = $proLeyenda3;

        return $this;
    }

    public function getProLeyenda4(): ?string
    {
        return $this->proLeyenda4;
    }

    public function setProLeyenda4(string $proLeyenda4): self
    {
        $this->proLeyenda4 = $proLeyenda4;

        return $this;
    }

    public function getProCpto1(): ?string
    {
        return $this->proCpto1;
    }

    public function setProCpto1(string $proCpto1): self
    {
        $this->proCpto1 = $proCpto1;

        return $this;
    }

    public function getProCpto2(): ?string
    {
        return $this->proCpto2;
    }

    public function setProCpto2(string $proCpto2): self
    {
        $this->proCpto2 = $proCpto2;

        return $this;
    }

    public function getProCpto3(): ?string
    {
        return $this->proCpto3;
    }

    public function setProCpto3(string $proCpto3): self
    {
        $this->proCpto3 = $proCpto3;

        return $this;
    }

    public function getProCopias(): ?int
    {
        return $this->proCopias;
    }

    public function setProCopias(int $proCopias): self
    {
        $this->proCopias = $proCopias;

        return $this;
    }

    public function getProBuscaf(): ?string
    {
        return $this->proBuscaf;
    }

    public function setProBuscaf(string $proBuscaf): self
    {
        $this->proBuscaf = $proBuscaf;

        return $this;
    }

    public function getProCajachica(): ?string
    {
        return $this->proCajachica;
    }

    public function setProCajachica(string $proCajachica): self
    {
        $this->proCajachica = $proCajachica;

        return $this;
    }

    public function getProInstit(): ?int
    {
        return $this->proInstit;
    }

    public function setProInstit(int $proInstit): self
    {
        $this->proInstit = $proInstit;

        return $this;
    }

    public function getProCopias2(): ?int
    {
        return $this->proCopias2;
    }

    public function setProCopias2(int $proCopias2): self
    {
        $this->proCopias2 = $proCopias2;

        return $this;
    }

    public function getProProcancela(): ?string
    {
        return $this->proProcancela;
    }

    public function setProProcancela(string $proProcancela): self
    {
        $this->proProcancela = $proProcancela;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getProBloqfech(): ?string
    {
        return $this->proBloqfech;
    }

    public function setProBloqfech(string $proBloqfech): self
    {
        $this->proBloqfech = $proBloqfech;

        return $this;
    }

    public function getLeyenda(): ?string
    {
        return $this->leyenda;
    }

    public function setLeyenda(?string $leyenda): self
    {
        $this->leyenda = $leyenda;

        return $this;
    }

    public function getProCatexcl(): ?string
    {
        return $this->proCatexcl;
    }

    public function setProCatexcl(?string $proCatexcl): self
    {
        $this->proCatexcl = $proCatexcl;

        return $this;
    }

    public function getProReporte(): ?string
    {
        return $this->proReporte;
    }

    public function setProReporte(string $proReporte): self
    {
        $this->proReporte = $proReporte;

        return $this;
    }

    public function getProDestinatario(): ?string
    {
        return $this->proDestinatario;
    }

    public function setProDestinatario(string $proDestinatario): self
    {
        $this->proDestinatario = $proDestinatario;

        return $this;
    }

    public function getProCodigo(): ?string
    {
        return $this->proCodigo;
    }



    
}
