<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * ChequeEstado
 *
 * @ORM\Table(name="cheque_estado")
 * @ORM\Entity
 */
class ChequeEstado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="nro", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $nro;

    /**
     * @var integer
     *
     * @ORM\Column(name="asiento", type="bigint", nullable=false)
     */
    private $asiento;

    /**
     * @var integer
     *
     * @ORM\Column(name="legalizacion", type="integer", nullable=false)
     */
    private $legalizacion;

    /**
     * @var float
     *
     * @ORM\Column(name="importe", type="float", precision=24, scale=2, nullable=false)
     */
    private $importe;

    /**
     * @var string
     *
     * @ORM\Column(name="banco", type="string", length=20, nullable=false)
     */
    private $banco;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fec_diferida", type="datetime", nullable=false)
     */
    private $fec_diferida;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fec_comprobante", type="datetime", nullable=false)
     */
    private $fec_comprobante;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=50, nullable=true)
     */
    private $estado;

    public function getId(): ?int
    {
        return $this->nro;
    }

    public function getNro(): ?int
    {
        return $this->nro;
    }

    public function setNro(?int $nro): self
    {
        $this->nro = $nro;

        return $this;
    }

    public function getAsiento(): ?string
    {
        return $this->asiento;
    }

    public function setAsiento(?string $asiento): self
    {
        $this->asiento = $asiento;

        return $this;
    }

    public function getLegalizacion(): ?int
    {
        return $this->legalizacion;
    }

    public function setLegalizacion(?int $legalizacion): self
    {
        $this->legalizacion = $legalizacion;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(?float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getBanco(): ?string
    {
        return $this->banco;
    }

    public function setBanco(?string $banco): self
    {
        $this->banco = $banco;

        return $this;
    }

    public function getFecDiferida(): ?\DateTimeInterface
    {
        return $this->fec_diferida;
    }

    public function setFecDiferida(?\DateTimeInterface $fec_diferida): self
    {
        $this->fec_diferida = $fec_diferida;

        return $this;
    }

    public function getFecComprobante(): ?\DateTimeInterface
    {
        return $this->fec_comprobante;
    }

    public function setFecComprobante(?\DateTimeInterface $fec_comprobante): self
    {
        $this->fec_comprobante = $fec_comprobante;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

}
