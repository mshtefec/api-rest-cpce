<?php

namespace App\Entity;

use App\Repository\AfiliadosTitulosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AfiliadosTitulosRepository::class)
 * @ORM\Entity
 * @ORM\Table(name="afiliados_titulos")
 */
class AfiliadosTitulos implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Afiliado", inversedBy="afi_titulos")
     * @ORM\JoinColumn(name="afiliado", referencedColumnName="afi_nrodoc", nullable=FALSE)
     */
    private $afiliado;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Titulo")
     * @ORM\JoinColumn(name="titulo", referencedColumnName="id")
     */
    private $titulo;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $matricula;

    public function getAfiliado(): ?Afiliado
    {
        return $this->afiliado;
    }

    public function setAfiliado(?Afiliado $afiliado): self
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    public function getTitulo(): ?Titulo
    {
        return $this->titulo;
    }

    public function setTitulo(?Titulo $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getMatricula(): ?int
    {
        return $this->matricula;
    }

    public function setMatricula(int $matricula): self
    {
        $this->matricula = $matricula;

        return $this;
    }

    public function jsonSerialize(): array {
        return [
            'afiliado' => $this->afiliado,
            'titulo' => $this->titulo,
            'matricula' => $this->matricula
        ];
    }

}