<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use App\Entity\Afiliado;
use App\Entity\Titulo;
use App\Entity\AfiliadosTitulos;
use App\Entity\ObraSocial;

class AfiliadoController extends AbstractController
{
    public function getAll(Request $request, PaginatorInterface $paginator): JsonResponse
    {
        // $page = $request->query->getInt('page', 1);
        // $item_per_page = 100;

        $dql = "
            SELECT a
            FROM App\Entity\Afiliado a
            WHERE a.afiTipdoc = 'DNI'
            ORDER BY a.afiNombre 
        ";

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql);

        $entities = $query->getResult();

        // $pagination = $paginator->paginate($query, $page, $item_per_page, array('wrap-queries' => true, 'distinct' => false));
        // $total = $pagination->getTotalItemCount();
        
        $data = [];
        // foreach ($pagination as $entity) {
        foreach ($entities as $entity) {
            $data[] = [
                'afi_tipdoc' => $entity->getAfiTipdoc(),
                'afi_nrodoc' => $entity->getAfiNrodoc(),
                'afi_cuit' => $entity->getAfiCuit(),
                'afi_nombre' => $entity->getAfiNombre(),
                'afi_fecnac' => $entity->getAfiFecnac(),
                'afi_sexo' => $entity->getAfiSexo(),
                'afi_civil' => $entity->getAfiCivil(),
                'afi_direccion' => $entity->getAfiDireccion(),
                'afi_localidad' => $entity->getAfiLocalidad(),
                'afi_provincia' => $entity->getAfiProvincia(),
                'afi_codpos' => $entity->getAfiCodpos(),
                'afi_telefono1' => $entity->getAfiTelefono1(),
                'afi_telefono2' => $entity->getAfiTelefono2(),
                'afi_mail' => $entity->getAfiMail(),
                'afi_mail_alternativo' => $entity->getAfiMailAlternativo(),
                'afi_familiares' => $entity->getAfiFamiliares()->toArray(),
                'afi_tarjetas' => $entity->getAfiTarjetas()->toArray(),
                'afi_titulos' => $entity->getAfiTitulos()->toArray(),
                'afi_ficha' => $entity->getAfiFicha(),
                'afi_tipo' => $entity->getAfitipo(),
                'afi_matricula' => $entity->getAfiMatricula(),
                'afi_zona' => $entity->getAfiZona(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Afiliado::class)->findOneBy(['afiNrodoc' => $id]);
       
        $data = [
            'afi_tipdoc' => $entity->getAfiTipdoc(),
            'afi_nrodoc' => $entity->getAfiNrodoc(),
            'afi_cuit' => $entity->getAfiCuit(),
            'afi_nombre' => $entity->getAfiNombre(),
            'afi_fecnac' => $entity->getAfiFecnac(),
            'afi_sexo' => $entity->getAfiSexo(),
            'afi_civil' => $entity->getAfiCivil(),
            'afi_direccion' => $entity->getAfiDireccion(),
            'afi_localidad' => $entity->getAfiLocalidad(),
            'afi_provincia' => $entity->getAfiProvincia(),
            'afi_codpos' => $entity->getAfiCodpos(),
            'afi_telefono1' => $entity->getAfiTelefono1(),
            'afi_telefono2' => $entity->getAfiTelefono2(),
            'afi_mail' => $entity->getAfiMail(),
            'afi_mail_alternativo' => $entity->getAfiMailAlternativo(),
            'afi_ficha' => $entity->getAfiFicha(),
            'afi_familiares' => $entity->getAfiFamiliares()->toArray(),
            'afi_titulos' => $entity->getAfiTitulos()->toArray(),
            'afi_tarjetas' => $entity->getAfiTarjetas()->toArray(),
            'afi_tipo' => $entity->getAfitipo(),
            'afi_matricula' => $entity->getAfiMatricula(),
            'afi_zona' => $entity->getAfiZona(),

        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {

            $afi_ficha = (!empty($params->afi_ficha)) ? $params->afi_ficha : null;
            $afi_zona = (!empty($params->afi_zona)) ? $params->afi_zona : null;
            $afi_nombre = (!empty($params->afi_nombre)) ? $params->afi_nombre : null;
            $afi_fecnac = (!empty($params->afi_fecnac)) ? $params->afi_fecnac : null;
            $afi_nrodoc = (!empty($params->afi_nrodoc)) ? $params->afi_nrodoc : null;
            $afi_cuit = (!empty($params->afi_cuit)) ? $params->afi_cuit : null;
            $afi_sexo = (!empty($params->afi_sexo)) ? $params->afi_sexo : null;
            $afi_civil = (!empty($params->afi_civil)) ? $params->afi_civil : null;
            $afi_direccion = (!empty($params->afi_direccion)) ? $params->afi_direccion : null;
            $afi_localidad = (!empty($params->afi_localidad)) ? $params->afi_localidad : null;
            $afi_provincia = (!empty($params->afi_provincia)) ? $params->afi_provincia : null;
            $afi_codpos = (!empty($params->afi_codpos)) ? $params->afi_codpos : null;
            $afi_mail = (!empty($params->afi_mail)) ? $params->afi_mail : null;
            $afi_mail_alternativo = (!empty($params->afi_mail_alternativo)) ? $params->afi_mail_alternativo : null;
            $afi_telefono1 = (!empty($params->afi_telefono1)) ? $params->afi_telefono1 : null;
            $afi_telefono2 = (!empty($params->afi_telefono2)) ? $params->afi_telefono2 : null;
            $afi_titulos = (!empty($params->afi_titulos)) ? $params->afi_titulos : null;
            
            if (
                !empty($afi_ficha) &&
                !empty($afi_zona) &&
                !empty($afi_nombre) &&
                !empty($afi_fecnac) && 
                !empty($afi_nrodoc) &&
                !empty($afi_sexo) && 
                !empty($afi_civil) && 
                !empty($afi_direccion) &&
                !empty($afi_localidad) &&
                !empty($afi_provincia) &&
                !empty($afi_codpos) &&
                !empty($afi_mail) && 
                !empty($afi_telefono1) && 
                !empty($afi_titulos)
            ) {
                $fecnac_date = new \DateTime($afi_fecnac->date);
                
                $entity = $this->getDoctrine()->getRepository(Afiliado::class)->findOneBy(['afiNrodoc' => $id]);
                $entity
                    ->setAfiFicha($afi_ficha)
                    ->setAfiZona($afi_zona)
                    ->setAfiNombre($afi_nombre)
                    ->setAfiFecnac($fecnac_date)
                    ->setAfiNrodoc($afi_nrodoc)
                    ->setAfiCuit($afi_cuit)
                    ->setAfiSexo($afi_sexo)
                    ->setAfiCivil($afi_civil)
                    ->setAfiDireccion($afi_direccion)
                    ->setAfiLocalidad($afi_localidad)
                    ->setAfiProvincia($afi_provincia)
                    ->setAfiCodpos($afi_codpos)
                    ->setAfiMail($afi_mail)
                    ->setAfiMailAlternativo($afi_mail_alternativo)
                    ->setAfiTelefono1($afi_telefono1)
                    ->setAfiTelefono2($afi_telefono2)
                ;

                if ($afi_titulos) {
                    foreach ($afi_titulos as $key => $afi_titulo) {
                        $afiliado = $entity;
                        $titulo = $this->getDoctrine()->getRepository(Titulo::class)->find($afi_titulo->titulo->id);
                        $matricula = $afi_titulo->matricula;

                        $afiTitulo = new AfiliadosTitulos();
                        $afiTitulo
                            ->setAfiliado($afiliado)
                            ->setTitulo($titulo)
                            ->setMatricula($matricula);
                        ;
                        $entity->addAfiTitulo($afiTitulo);
                    }
                }
                 
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar. Complete los campos'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {

            $afi_nrodoc = (!empty($params->afi_nrodoc)) ? $params->afi_nrodoc : null;
            $afi_tipdoc = (!empty($params->afi_tipdoc)) ? $params->afi_tipdoc : null;
            $afi_cuit = (!empty($params->afi_cuit)) ? $params->afi_cuit : null;
            $afi_nombre = (!empty($params->afi_nombre)) ? $params->afi_nombre : null;
            $afi_fecnac = (!empty($params->afi_fecnac)) ? $params->afi_fecnac : null;
            $afi_sexo = (!empty($params->afi_sexo)) ? $params->afi_sexo : null;
            $afi_civil = (!empty($params->afi_civil)) ? $params->afi_civil : null;
            $afi_direccion = (!empty($params->afi_direccion)) ? $params->afi_direccion : null;
            $afi_localidad = (!empty($params->afi_localidad)) ? $params->afi_localidad : null;
            $afi_provincia = (!empty($params->afi_provincia)) ? $params->afi_provincia : null;
            $afi_codpos = (!empty($params->afi_codpos)) ? $params->afi_codpos : null;
            $afi_telefono1 = (!empty($params->afi_telefono1)) ? $params->afi_telefono1 : null;
            $afi_telefono2 = (!empty($params->afi_telefono2)) ? $params->afi_telefono2 : null;
            $afi_mail = (!empty($params->afi_mail)) ? $params->afi_mail : null;
            $afi_mail_alternativo = (!empty($params->afi_mail_alternativo)) ? $params->afi_mail_alternativo : null;
            $titulos = (!empty($params->titulos)) ? $params->titulos : null;
            $afi_ficha = (!empty($params->afi_ficha)) ? $params->afi_ficha : null;
            $obraSocial = (!empty($params->obraSocial)) ? $params->obraSocial : null;
            $afi_tipo = (!empty($params->afi_tipo)) ? $params->afi_tipo : null;
            $afi_matricula = (!empty($params->afi_matricula)) ? $params->afi_matricula : 1;
            $afi_zona = (!empty($params->afi_zona)) ? $params->afi_zona : null;

            $validator = Validation::createValidator();
            $validate_email = $validator->validate($afi_mail, [
                new Email()
            ]);
            $validator2 = Validation::createValidator();
            $validate_email2 = $validator2->validate($afi_mail_alternativo, [
                new Email()
            ]);
            if (
                
                !empty($afi_nrodoc) &&
                !empty($afi_tipdoc) &&
                !empty($afi_mail) && count($validate_email) == 0 && 
                !empty($afi_mail_alternativo) && count($validate_email2) == 0 && 
                !empty($afi_nombre) &&
                !empty($afi_cuit) &&
                !empty($afi_fecnac) && 
                !empty($afi_sexo) && 
                !empty($afi_civil) && 
                !empty($afi_direccion) &&
                !empty($afi_localidad) &&
                !empty($afi_provincia) &&
                !empty($afi_codpos) &&
                !empty($afi_telefono1) && 
                !empty($afi_telefono2) &&
                !empty($titulos) &&
                !empty($afi_ficha) &&
                !empty($obraSocial) &&
                !empty($afi_tipo) &&
                !empty($afi_matricula) &&
                !empty($afi_zona)
                
            ) {
               
                $temp_afi_fecnac = new \DateTime($afi_fecnac);
                $entity = new Afiliado();
                $temObraSocial = $this->getDoctrine()->getRepository(ObraSocial::class)->findOneBy(['id' => $obraSocial]);
                $entity
                ->setAfiNrodoc($afi_nrodoc)
                ->setAfiTipdoc($afi_tipdoc)
                ->setAfiNombre($afi_nombre)
                ->setAfiCuit($afi_cuit)
                ->setAfiFecnac($temp_afi_fecnac)
                ->setAfiSexo($afi_sexo)
                ->setAfiCivil($afi_civil)
                ->setAfiDireccion($afi_direccion)
                ->setAfiLocalidad($afi_localidad)
                ->setAfiProvincia($afi_provincia)
                ->setAfiCodpos($afi_codpos)
                ->setAfiTelefono1($afi_telefono1)
                ->setAfiTelefono2($afi_telefono2)
                ->setAfiMail($afi_mail)
                ->setAfiMailAlternativo($afi_mail_alternativo)
                ->setAfiFicha($afi_ficha)
                ->setObrasocial($temObraSocial)
                ->setAfiTipo($afi_tipo)
                ->setAfiMatricula($afi_matricula)
                ->setAfiZona($afi_zona)
                ;
                
                $exist_user = $this->getDoctrine()->getRepository(Afiliado::class)->findBy(
                    array(
                        'afiNrodoc' => $afi_nrodoc
                    )
                );

                if (count($exist_user) == 0) {

                $array = json_decode(json_encode($titulos), true);
                foreach ($array as $key => $i) {
                    if(!empty($titulos)){
                        $temTitulo = $this->getDoctrine()->getRepository(Titulo::class)->findOneBy(['nombre' => $i['nombre']]);
                        $entity->addTitulo($temTitulo);
                    } 
                } 
                 
                    $em =  $this->getDoctrine()->getManager();
                    $em->persist($entity);
                    $em->flush();
                
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Elemento creado.',
                        'User' => $entity
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ya existe ese usuario.'
                    ];   
                }

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function getBy($search, Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        
        // OPCION 1
        $dql = "
            SELECT 
                a
            FROM 
                App\Entity\Afiliado a
            WHERE 
                a.afiNrodoc LIKE '%" . $search . "%'
            ORDER BY a.afiNrodoc DESC 
        ";
        $query = $em->createQuery($dql);


        $data = [];
        foreach ($query->getResult() as $entity) {
            $data[] = [
                'afi_tipdoc' => $entity->getAfiTipdoc(),
                'afi_nrodoc' => $entity->getAfiNrodoc(),
                'afi_cuit' => $entity->getAfiCuit(),
                'afi_nombre' => $entity->getAfiNombre(),
                'afi_fecnac' => $entity->getAfiFecnac(),
                'afi_sexo' => $entity->getAfiSexo(),
                'afi_civil' => $entity->getAfiCivil(),
                'afi_direccion' => $entity->getAfiDireccion(),
                'afi_localidad' => $entity->getAfiLocalidad(),
                'afi_provincia' => $entity->getAfiProvincia(),
                'afi_codpos' => $entity->getAfiCodpos(),
                'afi_telefono1' => $entity->getAfiTelefono1(),
                'afi_telefono2' => $entity->getAfiTelefono2(),
                'afi_mail' => $entity->getAfiMail(),
                'afi_mail_alternativo' => $entity->getAfiMailAlternativo(),
                'titulos' => $entity->getTitulos(),
                'afi_ficha' => $entity->getAfiFicha(),
                'obraSocial' => $entity->getObraSocial(),
                'afi_tipo' => $entity->getAfitipo(),
                'afi_matricula' => $entity->getAfiMatricula(),
                'afi_zona' => $entity->getAfiZona(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }
}