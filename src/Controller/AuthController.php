<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

use App\Entity\User;
use App\Services\JwtAuth;

class AuthController extends AbstractController
{

    /**
     * @Route("/login", name="login", methods={"POST"})
     */
    public function login(Request $request, JwtAuth $jwt_auth): JsonResponse
    {
        
        $json = $request->getContent();
       
        $params = json_decode($json); 

        $data = [
            'status' => 'error',
            'code' => 400,
            'message' => "ups, algo no salio como se esperaba.",
            'json' => $json
        ];
        
        if ($json != null) {
            
         
            $email = (!empty($params->email)) ? $params->email : null;
            $password = (!empty($params->password)) ? $params->password : null;
            $get_token = (!empty($params->get_token)) ? $params->get_token : null;
            
            $validator = Validation::createValidator();
            $validate_email = $validator->validate($email, [
                new Email()
            ]);

            if (
               
                !empty($email) && count($validate_email) == 0 && 
                !empty($password) 
            ) {
                
                $pwd = hash('sha256', $password);

                if ($get_token) {
                    $signup = $jwt_auth->signup($email, $pwd, $get_token);
                } else {
                    $signup = $jwt_auth->signup($email, $pwd);
                }

                return new JsonResponse($signup);
            
            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Contraseña o Email incorrectos.'
                ];
            }

        }

        return new JsonResponse($data);
    }

    public function register(Request $request): JsonResponse
    {
        
        $json = $request->query->get('json', null);
        
        $params = json_decode($json);

        if ($json != null) {
            
            $username = (!empty($params->username)) ? $params->username : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $password = (!empty($params->password)) ? $params->password : null;
            
            $validator = Validation::createValidator();
            $validate_email = $validator->validate($email, [
                new Email()
            ]);

            if (
                !empty($username) && 
                !empty($email) && count($validate_email) == 0 && 
                !empty($password) 
            ) {
                $created_at = new \DateTime("now");
                $updated_at = new \DateTime("now");

                $pwd = hash('sha256', $password);
                
                $entity = new User();
                $entity
                    ->setUsername($username)
                    ->setEmail($email)
                    ->setPassword($pwd)
                ;

                $exist_user = $this->repository->findBy(
                    array(
                        'email' => $email
                    )
                );

                if (count($exist_user) == 0) {
                    $this->repository->add($entity);
                
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Elemento creado.',
                        'User' => $entity
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ya existe ese usuario.'
                    ];   
                }

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }
}