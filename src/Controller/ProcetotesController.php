<?php
namespace App\Controller;

use App\Entity\Procetote;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Procetotes;

class ProcetotesController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Procetotes::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
               // 'id' => $entity->getId(),
                'pto_codpro' => $entity->getPto_codpro(),
                'pto_nropla' => $entity->getPto_nropla(),
                'pto_tipmov' => $entity->getPto_tipmov(),
                'pto_item' => $entity->getPto_item(),
                'pto_debe' => $entity->getPto_debe(),
                'pto_haber' => $entity->getPto_haber(),
                'pto_tabla' => $entity->getPto_tabla(),
                'pto_campo' => $entity->getPto_campo(),
                'pto_tipcam' => $entity->getPto_tipcam(),
                'pto_valor' => $entity->getPto_valor(),
                'pto_formula' => $entity->getPto_formula(),
                'pto_lote' => $entity->getPto_lote(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Procetotes::class)->findOneBy(['pto_codpro' => $id]);

        $data = [
                'pto_codpro' => $entity->getPto_codpro(),
                'pto_nropla' => $entity->getPto_nropla(),
                'pto_tipmov' => $entity->getPto_tipmov(),
                'pto_item' => $entity->getPto_item(),
                'pto_debe' => $entity->getPto_debe(),
                'pto_haber' => $entity->getPto_haber(),
                'pto_tabla' => $entity->getPto_tabla(),
                'pto_campo' => $entity->getPto_campo(),
                'pto_tipcam' => $entity->getPto_tipcam(),
                'pto_valor' => $entity->getPto_valor(),
                'pto_formula' => $entity->getPto_formula(),
                'pto_lote' => $entity->getPto_lote(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $pto_codpro = (!empty($params->pto_codpro)) ? $params->pto_codpro : null;
            $pto_nropla = (!empty($params->pto_nropla)) ? $params->pto_nropla : null;
            $pto_tipmov = (!empty($params->pto_tipmov)) ? $params->pto_tipmov : null;
            $pto_item = (!empty($params->pto_item)) ? $params->pto_item : null;
            $pto_debe = (!empty($params->pto_debe)) ? $params->pto_debe : null;
            $pto_haber = (!empty($params->pto_haber)) ? $params->pto_haber : null;
            $pto_tabla = (!empty($params->pto_tabla)) ? $params->pto_tabla : null;
            $pto_campo = (!empty($params->pto_campo)) ? $params->pto_campo : null;
            $pto_tipcam = (!empty($params->pto_tipcam)) ? $params->pto_tipcam : null;
            $pto_valor = (!empty($params->pto_valor)) ? $params->pto_valor : null;
            $pto_formula = (!empty($params->pto_formula)) ? $params->pto_formula : null;
            $pto_lote = (!empty($params->pto_lote)) ? $params->pto_lote : null;

            
            if (
                !empty($pto_codpro) && 
                !empty($pto_nropla) &&
                !empty($pto_tipmov) &&
                !empty($pto_item) &&
                !empty($pto_debe) &&
                !empty($pto_haber) &&
                !empty($pto_tabla) &&
                !empty($pto_campo) &&
                !empty($pto_tipcam) &&
                !empty($pto_valor) &&
                !empty($pto_formula) &&
                !empty($pto_lote)
            ) {
                $created_at = new \DateTime("now");
                $updated_at = new \DateTime("now");

                $entity = new Procetote();
                $entity
                    ->setPtoCodpro($pto_codpro)
                    ->setPtoNropla($pto_nropla) 
                    ->setPtoTipmov($pto_tipmov)
                    ->setPtoItem($pto_item) 
                    ->setPtoDebe($pto_debe) 
                    ->setPtoHaber($pto_haber) 
                    ->setPtoTabla($pto_tabla) 
                    ->setPtoCampo($pto_campo)
                    ->setPtoTipcam($pto_tipcam) 
                    ->setPtoValor($pto_valor) 
                    ->setPtoFormula($pto_formula)
                    ->setPtoLote($pto_lote)
                    ;

                $em =  $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento creado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $pto_codpro = (!empty($params->pto_codpro)) ? $params->pto_codpro : null;
            $pto_nropla = (!empty($params->pto_nropla)) ? $params->pto_nropla : null;
            $pto_tipmov = (!empty($params->pto_tipmov)) ? $params->pto_tipmov : null;
            $pto_item = (!empty($params->pto_item)) ? $params->pto_item : null;
            $pto_debe = (!empty($params->pto_debe)) ? $params->pto_debe : null;
            $pto_haber = (!empty($params->pto_haber)) ? $params->pto_haber : null;
            $pto_tabla = (!empty($params->pto_tabla)) ? $params->pto_tabla : null;
            $pto_campo = (!empty($params->pto_campo)) ? $params->pto_campo : null;
            $pto_tipcam = (!empty($params->pto_tipcam)) ? $params->pto_tipcam : null;
            $pto_valor = (!empty($params->pto_valor)) ? $params->pto_valor : null;
            $pto_formula = (!empty($params->pto_formula)) ? $params->pto_formula : null;
            $pto_lote = (!empty($params->pto_lote)) ? $params->pto_lote : null;
            
            if (
                !empty($pto_codpro) && 
                !empty($pto_nropla) &&
                !empty($pto_tipmov) &&
                !empty($pto_item) &&
                !empty($pto_debe) &&
                !empty($pto_haber) &&
                !empty($pto_tabla) &&
                !empty($pto_campo) &&
                !empty($pto_tipcam) &&
                !empty($pto_valor) &&
                !empty($pto_formula) &&
                !empty($pto_lote)
            ) {
                
                $updated_at = new \DateTime("now");

                $entity = $this->getDoctrine()->getRepository(Procetote::class)->findOneBy(['id' => $id]);
                $entity
                ->setPtoCodpro($pto_codpro)
                ->setPtoNropla($pto_nropla) 
                ->setPtoTipmov($pto_tipmov)
                ->setPtoItem($pto_item) 
                ->setPtoDebe($pto_debe) 
                ->setPtoHaber($pto_haber) 
                ->setPtoTabla($pto_tabla) 
                ->setPtoCampo($pto_campo)
                ->setPtoTipcam($pto_tipcam) 
                ->setPtoValor($pto_valor) 
                ->setPtoFormula($pto_formula)
                ->setPtoLote($pto_lote)
                ;

                $em =  $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }
}