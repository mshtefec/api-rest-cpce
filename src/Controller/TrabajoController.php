<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Trabajo;

class TrabajoController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $data = [];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Trabajo::class)->findOneBy(['id' => $id]);

        $data = [
            'id' => $entity->getId(),
            'tra_nroasi' => $entity->getNroAsiento(),
            'tra_nrolegali' => $entity->getNroLegalizacion(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOneByLegalizacion($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Trabajo::class)->findOneBy(['nroLegalizacion' => $id]);

        $data = [
            'id' => $entity->getId(),
            'tra_nroasi' => $entity->getNroAsiento(),
            'tra_nrolegali' => $entity->getNroLegalizacion(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

}