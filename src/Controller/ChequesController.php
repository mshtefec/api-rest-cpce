<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Cheques;

class ChequesController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Cheques::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'FECHA' => $entity->getFecha(),
                'INSTITUT' => $entity->getInstit(),
                'PROCESO' => $entity->getProceso(),
                'COMPROB' => $entity->getComprob(),
                'ASIENTO' => $entity->getAsiento(),
                'CUENTA' => $entity->getCuenta(),
                'MATRICULA' => $entity->getMatricula(),
                'ZONA' => $entity->getZona(),
                'IMPORTE' => $entity->getImporte(),
                'COLUMNA' => $entity->getColumna(),
                'TIPO' => $entity->getTipo(),
                'ESTADO' => $entity->getEstado(),
                'MADRE' => $entity->getMadre(),
                'CAMPO1' => $entity->getCampo1(),
                'FCONCILIA' => $entity->getFconcilia(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Cheques::class)->findOneBy(['fecha' => $id]);

        $data = [
              'FECHA' => $entity->getFecha(),
              'INSTITUT' => $entity->getInstit(),
              'PROCESO' => $entity->getProceso(),
              'COMPROB' => $entity->getComprob(),
              'ASIENTO' => $entity->getAsiento(),
              'CUENTA' => $entity->getCuenta(),
              'MATRICULA' => $entity->getMatricula(),
              'ZONA' => $entity->getZona(),
              'IMPORTE' => $entity->getImporte(),
              'COLUMNA' => $entity->getColumna(),
              'TIPO' => $entity->getTipo(),
              'ESTADO' => $entity->getEstado(),
              'MADRE' => $entity->getMadre(),
              'CAMPO1' => $entity->getCampo1(),
              'FCONCILIA' => $entity->getFconcilia(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            
            $fecha = (!empty($params->fecha)) ? $params->fecha : null;
            $institut = (!empty($params->institut)) ? $params->institut : null;
            $proceso = (!empty($params->proceso)) ? $params->proceso : null;
            $comprob = (!empty($params->comprob)) ? $params->comprob : null;
            $asiento= (!empty($params->asiento)) ? $params->asiento : null;
            $cuenta = (!empty($params->cuenta)) ? $params->cuenta : null;
            $matricula = (!empty($params->matricula)) ? $params->matricula : null;
            $zona= (!empty($params->zona)) ? $params->zona : null;
            $importe = (!empty($params->importe)) ? $params->importe : null;
            $columna = (!empty($params->columna)) ? $params->columna : null;
            $tipo = (!empty($params->tipo)) ? $params->tipo : null;
            $estado = (!empty($params->estado)) ? $params->estado : null;
            $madre = (!empty($params->madre)) ? $params->madre : null;
            $campo1 = (!empty($params->campo1)) ? $params->campo1 : null;
            $fconcilia = (!empty($params->fconcilia)) ? $params->fconcilia : null; 
            if (
                !empty($fecha) &&
                !empty($institut) &&
                !empty($proceso) &&
                !empty($comprob) &&
                !empty($asiento) &&
                !empty($cuenta) &&
                !empty($matricula) &&
                !empty($zona) &&
                !empty($importe) &&
                !empty($columna) &&
                !empty($tipo) &&
                !empty($estado) &&
                !empty($madre) &&
                !empty($campo1) &&
                !empty($fconcilia) 
            ) {
                $created_at = new \DateTime("now");
                $updated_at = new \DateTime("now");

                $entity = new Cheques();
                $entity
                    ->setFecha($fecha)
                    ->setInstit($institut)
                    ->setProceso($proceso)
                    ->setComprob($comprob)
                    ->setAsiento($asiento)
                    ->setCuenta($cuenta)
                    ->setMatricula($matricula)
                    ->setZona($zona)
                    ->setImporte($importe)
                    ->setColumna($columna)
                    ->setTipo($tipo)
                    ->setEstado($estado)
                    ->setMadre($madre)
                    ->setCampo1($campo1)
                    ->setFconcilia($fconcilia)
                    ;

                $em =  $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento creado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $fecha = (!empty($params->fecha)) ? $params->fecha : null;
            $institut = (!empty($params->institut)) ? $params->institut : null;
            $proceso = (!empty($params->proceso)) ? $params->proceso : null;
            $comprob = (!empty($params->comprob)) ? $params->comprob : null;
            $asiento= (!empty($params->asiento)) ? $params->asiento : null;
            $cuenta = (!empty($params->cuenta)) ? $params->cuenta : null;
            $matricula = (!empty($params->matricula)) ? $params->matricula : null;
            $zona= (!empty($params->zona)) ? $params->zona : null;
            $importe = (!empty($params->importe)) ? $params->importe : null;
            $columna = (!empty($params->columna)) ? $params->columna : null;
            $tipo = (!empty($params->tipo)) ? $params->tipo : null;
            $estado = (!empty($params->estado)) ? $params->estado : null;
            $madre = (!empty($params->madre)) ? $params->madre : null;
            $campo1 = (!empty($params->campo1)) ? $params->campo1 : null;
            $fconcilia = (!empty($params->fconcilia)) ? $params->fconcilia : null; 
            
            if (
                !empty($fecha) &&
                !empty($institut) &&
                !empty($proceso) &&
                !empty($comprob) &&
                !empty($asiento) &&
                !empty($cuenta) &&
                !empty($matricula) &&
                !empty($zona) &&
                !empty($importe) &&
                !empty($columna) &&
                !empty($tipo) &&
                !empty($estado) &&
                !empty($madre) &&
                !empty($campo1) &&
                !empty($fconcilia) 
            ) {
                
                $updated_at = new \DateTime("now");

                $entity = $this->getDoctrine()->getRepository(Cheques::class)->findOneBy(['id' => $id]);
                $entity
                ->setFecha($fecha)
                ->setInstit($institut)
                ->setProceso($proceso)
                ->setComprob($comprob)
                ->setAsiento($asiento)
                ->setCuenta($cuenta)
                ->setMatricula($matricula)
                ->setZona($zona)
                ->setImporte($importe)
                ->setColumna($columna)
                ->setTipo($tipo)
                ->setEstado($estado)
                ->setMadre($madre)
                ->setCampo1($campo1)
                ->setFconcilia($fconcilia)
                ;

                $em =  $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }
}