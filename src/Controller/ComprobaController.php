<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Comproba;

class ComprobaController extends AbstractController
{
    public function getAll(Request $request, PaginatorInterface $paginator): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Comproba::class)->findAll();
        // $page = $request->query->getInt('page', 1);
        // $item_per_page = 100;

        // $dql = "
        //     SELECT c
        //     FROM App\Entity\Comproba c
        //     ORDER BY c.comNroasi DESC 
        // ";

        // $em = $this->getDoctrine()->getManager();
        // $query = $em->createQuery($dql);

        // $pagination = $paginator->paginate($query, $page, $item_per_page, array('wrap-queries' => true, 'distinct' => false));
        // $total = $pagination->getTotalItemCount();
        
        $data = [];
        foreach ($entities as $entity) {
        // foreach ($pagination as $entity) {
            $data[] = [
                'com_unegos' => $entity->getComUnegos(),
                'com_nrocli' => $entity->getComNrocli(),
                'com_nroasi' => $entity->getComNroasi(),
                'com_nrocom' => $entity->getComNrocom(),
                'com_nrodeleg' => $entity->getComNrodeleg(),
                'com_nroope' => $entity->getComNroope(),
                'com_tipdoc' => $entity->getComTipdoc(),
                'com_nrodoc' => $entity->getComNrodoc(),
                'com_titulo' => $entity->getComTitulo(),
                'com_matricula' => $entity->getComMatricula(),
                'com_zeta' => $entity->getComZeta(),
                'com_total' => $entity->getComTotal(),
                'com_proceso' => $entity->getComProceso(),
                'com_concepto1' => $entity->getComConcepto1(),
                'com_concepto2' => $entity->getComConcepto2(),
                'com_concepto3' => $entity->getComConcepto3(),
                'com_fecha' => $entity->getComFecha(),
                'com_fecalt' => $entity->getComFecalt(),
                'com_fecven' => $entity->getComFecven(),
                'com_fecpag' => $entity->getComFecpag(),
                'com_estado' => $entity->getComEstado(),
                'com_tarea' => $entity->getComTarea(),
                'com_nrotrabajo' => $entity->getComNrotrabajo(),
                'com_nrolegali' => $entity->getComNrolegali(),
                'com_destinatario' => $entity->getComDestinatario(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getAllByCliAndProcess($nrocli = 4, $process = '02R120'): JsonResponse
    {
        // $entities = $this->getDoctrine()->getRepository(Comproba::class)->findAll();
        $entities = $this->getDoctrine()->getRepository(Comproba::class)->findBy([
            'comUnegos' => '2',
            'comNrocli' => $nrocli,
            'comProceso' => $process
            ], 
            ['comNroasi' => 'DESC']
        );
        // $page = $request->query->getInt('page', 1);
        // $item_per_page = 10000;

        // $dql = "
        //     SELECT 
        //         c
        //     FROM 
        //         App\Entity\Comproba c
        //     WHERE 
        //         c.comUnegos = 2 AND
        //         c.comNrocli = 4 AND
        //         c.comProceso = '02R120'
        //     ORDER BY c.comNroasi DESC 
        // ";

        // $em = $this->getDoctrine()->getManager();
        // $query = $em->createQuery($dql);

        // $pagination = $paginator->paginate($query, $page, $item_per_page, array('wrap-queries' => true, 'distinct' => false));
        // $total = $pagination->getTotalItemCount();
        
        $data = [];
        foreach ($entities as $entity) {
        // foreach ($pagination as $entity) {
            $data[] = [
                'com_unegos' => $entity->getComUnegos(),
                'com_nrocli' => $entity->getComNrocli(),
                'com_nroasi' => $entity->getComNroasi(),
                'com_nrocom' => $entity->getComNrocom(),
                'com_nrodeleg' => $entity->getComNrodeleg(),
                'com_nroope' => $entity->getComNroope(),
                'com_tipdoc' => $entity->getComTipdoc(),
                'com_nrodoc' => $entity->getComNrodoc(),
                'com_titulo' => $entity->getComTitulo(),
                'com_matricula' => $entity->getComMatricula(),
                'com_zeta' => $entity->getComZeta(),
                'com_total' => $entity->getComTotal(),
                'com_proceso' => $entity->getComProceso(),
                'com_concepto1' => $entity->getComConcepto1(),
                'com_concepto2' => $entity->getComConcepto2(),
                'com_concepto3' => $entity->getComConcepto3(),
                'com_fecha' => $entity->getComFecha(),
                'com_fecalt' => $entity->getComFecalt(),
                'com_fecven' => $entity->getComFecven(),
                'com_fecpag' => $entity->getComFecpag(),
                'com_estado' => $entity->getComEstado(),
                'com_tarea' => $entity->getComTarea(),
                'com_nrotrabajo' => $entity->getComNrotrabajo(),
                'com_nrolegali' => $entity->getComNrolegali(),
                'com_destinatario' => $entity->getComDestinatario(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Comproba::class)->findOneBy(['comNroasi' => $id]);

        $data = [
            'com_unegos' => $entity->getComUnegos(),
            'com_nrocli' => $entity->getComNrocli(),
            'com_nroasi' => $entity->getComNroasi(),
            'com_nrocom' => $entity->getComNrocom(),
            'com_nrodeleg' => $entity->getComNrodeleg(),
            'com_nroope' => $entity->getComNroope(),
            'com_tipdoc' => $entity->getComTipdoc(),
            'com_nrodoc' => $entity->getComNrodoc(),
            'com_titulo' => $entity->getComTitulo(),
            'com_matricula' => $entity->getComMatricula(),
            'com_zeta' => $entity->getComZeta(),
            'com_total' => $entity->getComTotal(),
            'com_proceso' => $entity->getComProceso(),
            'com_concepto1' => $entity->getComConcepto1(),
            'com_concepto2' => $entity->getComConcepto2(),
            'com_concepto3' => $entity->getComConcepto3(),
            'com_fecha' => $entity->getComFecha(),
            'com_fecalt' => $entity->getComFecalt(),
            'com_fecven' => $entity->getComFecven(),
            'com_fecpag' => $entity->getComFecpag(),
            'com_estado' => $entity->getComEstado(),
            'com_tarea' => $entity->getComTarea(),
            'com_nrotrabajo' => $entity->getComNrotrabajo(),
            'com_nrolegali' => $entity->getComNrolegali(),
            'com_destinatario' => $entity->getComDestinatario(),

        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    // public function add(Request $request): JsonResponse
    // {
        
    //     $json = $request->getContent();
    //     $params = json_decode($json);

    //     if ($json != null) {
            
            
    //           // 'id' => $entity->getId(), 
    //         $comUnegos = (!empty($params->comUnegos)) ? $params->comUnegos : null;
    //         $comNrodeleg = (!empty($params->comNrodeleg)) ? $params->comNrodeleg : null;
    //         $comProceso = (!empty($params->comProceso)) ? $params->comProceso : null;
    //         $comNrocli = (!empty($params->comNrocli)) ? $params->comNrocli : null;
    //         $comNrocom = (!empty($params->comNrocom)) ? $params->comNrocom : null;
    //         $comPreimpreso = (!empty($params->comPreimpreso)) ? $params->comPreimpreso : null;
    //         $comNroope = (!empty($params->comNroope)) ? $params->comNroope : null;
    //         $comFecha= (!empty($params->comFecha)) ? $params->comFecha : null;
    //         $comTitulo = (!empty($params->comTitulo)) ? $params->comTitulo : null;
    //         $comMatricula = (!empty($params->comMatricula)) ? $params->comMatricula : null;
    //         $comSubcuenta = (!empty($params->comSubcuenta)) ? $params->comSubcuenta : null;
    //         $comTipdoc = (!empty($params->comTipdoc)) ? $params->comTipdoc : null;
    //         $comNrodoc = (!empty($params->comNrodoc)) ? $params->comNrodoc : null;
    //         $comTipcmt = (!empty($params->comTipcmt)) ? $params->comTipcmt : null;
    //         $comNrocmt = (!empty($params->comNrocmt)) ? $params->comNrocmt : null;
    //         $comNrocuo = (!empty($params->comNrocuo)) ? $params->comNrocuo : null;
    //         $comNropre = (!empty($params->comNropre)) ? $params->comNropre : null;
    //         $comTipo = (!empty($params->comTipo)) ? $params->comTipo : null;
    //         $comLote = (!empty($params->comLote)) ? $params->comLote : null;
    //         $comCaja = (!empty($params->comCaja)) ? $params->comCaja : null;
    //         $comNroreg = (!empty($params->comNroreg)) ? $params->comNroreg : null;
    //         $comZeta = (!empty($params->comZeta)) ? $params->comZeta : null;
    //         $comTotal = (!empty($params->comTotal)) ? $params->comTotal : null;
    //         $comTipanu = (!empty($params->comTipanu)) ? $params->comTipanu : null;
    //         $comPunanu = (!empty($params->comPunanu)) ? $params->comPunanu : null;
    //         $comNroanu = (!empty($params->comNroanu)) ? $params->comNroanu : null;
    //         $comFecalt = (!empty($params->comFecalt)) ? $params->comFecalt : null;
    //         $comEstado = (!empty($params->comEstado)) ? $params->comEstado : null;
    //         $comFecven = (!empty($params->comFecven)) ? $params->comFecven : null;
    //         $comFecpag = (!empty($params->comFecpag)) ? $params->comFecpag : null;
    //         $comImppag = (!empty($params->comImppag)) ? $params->comImppag : null;
    //         $comTipmov = (!empty($params->comTipmov)) ? $params->comTipmov : null;
    //         $comCoeficiente = (!empty($params->comCoeficiente)) ? $params->comCoeficiente : null;
    //         $comConcepto1 = (!empty($params->comConcepto1)) ? $params->comConcepto1 : null;
    //         $comConcepto2 = (!empty($params->comConcepto2)) ? $params->comConcepto2 : null;
    //         $comConcepto3 = (!empty($params->comConcepto3)) ? $params->comConcepto3 : null;
    //         $comLeyenda = (!empty($params->comLeyenda)) ? $params->comLeyenda : null;
    //         $comRefano = (!empty($params->comRefano)) ? $params->comRefano : null;
    //         $comRefmes = (!empty($params->comRefmes)) ? $params->comRefmes : null;
    //         $comNroasi = (!empty($params->comNroasi)) ? $params->comNroasi : null;
    //         $comTarea = (!empty($params->comTarea)) ? $params->comTarea : null;
    //         $comNrotrabajo = (!empty($params->comNrotrabajo)) ? $params->comNrotrabajo : null;
    //         $comAsigrupal = (!empty($params->comAsigrupal)) ? $params->comAsigrupal : null;
    //         $comNrolegali = (!empty($params->comNrolegali)) ? $params->comNrolegali : null;
    //         $comDestinatario = (!empty($params->comDestinatario)) ? $params->comDestinatario : null;
            
    //         if (
    //             !empty($comUnegos) &&
    //             !empty($comNrodeleg) &&
    //             !empty($comProceso) &&
    //             !empty($comNrocli) &&
    //             !empty($comNrocom) &&
    //             !empty($comPreimpreso) &&
    //             !empty($comNroope) &&
    //             !empty($comFecha) &&
    //             !empty($comTitulo) &&
    //             !empty($comMatricula) &&
    //             !empty($comSubcuenta) &&
    //             !empty($comTipdoc) &&
    //             !empty($comNrodoc) &&
    //             !empty($comTipcmt) &&
    //             !empty($comNrocmt) &&
    //             !empty($comNrocuo) &&
    //             !empty($comNropre) &&
    //             !empty($comTipo) &&
    //             !empty($comLote) &&
    //             !empty($comCaja) &&
    //             !empty($comNroreg) &&
    //             !empty($comZeta) &&
    //             !empty($comTotal) &&
    //             !empty($comTipanu) &&
    //             !empty($comPunanu) &&
    //             !empty($comNroanu) &&
    //             !empty($comFecalt) &&
    //             !empty($comEstado) &&
    //             !empty($comFecven) &&
    //             !empty($comFecpag) &&
    //             !empty($comImppag) &&
    //             !empty($comTipmov) &&
    //             !empty($comCoeficiente) && 
    //             !empty($comConcepto1) &&
    //             !empty($comConcepto2) &&
    //             !empty($comConcepto3) &&
    //             !empty($comLeyenda) &&
    //             !empty($comRefano) &&
    //             !empty($comRefmes) &&
    //             !empty($comAsiant) &&
    //             !empty($comTarea) &&
    //             !empty($comNrotrabajo) && 
    //             !empty($comAsigrupal) &&
    //             !empty($comNrolegali) &&
    //             !empty($comDestinatario)  
    //         ) {

    //             $created_at = new \DateTime("now");
    //             $updated_at = new \DateTime("now");

    //             $entity = new Comproba();
    //             $entity
                  
    //             ->setComUnegos($comUnegos)
    //             ->setComNrodeleg($comNrodeleg)
    //             ->setComProceso($comProceso)
    //             ->setComNrocli($comNrocli)
    //             ->setComNrocom($comNrocom)
    //             ->setComPreimpreso($comPreimpreso)
    //             ->setComNroope($comNroope)
    //             ->setComFecha($comFecha)
    //             ->setComTitulo($comTitulo)
    //             ->setComMatricula($comMatricula)
    //             ->setComSubcuenta($comSubcuenta)
    //             ->setComTipdoc($comTipdoc)
    //             ->setComNrodoc($comNrodoc)
    //             ->setComTipcmt($comTipcmt)
    //             ->setComNrocmt($comNrocmt)
    //             ->setComNrocuo($comNrocuo)
    //             //->setPro_copias2($comNropre)
    //             ->setComNropre($comNropre)
    //             ->setComTipo($comTipo)
    //             ->setComLote($comLote)
    //             ->setComCaja($comCaja)
    //             ->setComNroreg($comNroreg)
    //             ->setComZeta($comZeta)
    //             ->setComTotal($comTotal)
    //             ->setComTipanu($comTipanu)
    //             ->setComPunanu($comPunanu)
    //             ->setComNroanu($comFecalt)
    //             ->setComFecalt($comEstado)
    //             ->setComEstado($comFecven)
    //             ->setComFecven($comFecpag)
    //             ->setComFecpag($comImppag)
    //             ->setComImppag($comImppag)
    //             ->setComTipmov($comTipmov)
    //             ->setComCoeficiente($comCoeficiente)
    //             ->setComNroasi($comNroasi)
    //             ->setComConcepto1($comConcepto1)
    //             ->setComConcepto2($comConcepto2)
    //             ->setComConcepto3($comConcepto3)
    //             ->setComLeyenda($comLeyenda)
    //             ->setComRefano($comRefano)
    //             ->setComRefmes($comRefmes)
    //             ->setComAsiant($comAsiant)
    //             ->setComTarea($comTarea)
    //             ->setComNrotrabajo($comNrotrabajo)
    //             ->setComAsigrupal($comAsigrupal)
    //             ->setComNrolegali($comNrolegali)
    //             ->setComDestinatario($comDestinatario)
    //             ;

    //             $em =  $this->getDoctrine()->getManager();
    //             $em->persist($entity);
    //             $em->flush();
                
    //             $data = [
    //                 'status' => 'success',
    //                 'code' => 200,
    //                 'message' => 'Elemento creado.'
    //             ];

    //         } else {
    //             $data = [
    //                 'status' => 'error',
    //                 'code' => 400,
    //                 'message' => 'El elemento no se ha podido crear.'
    //             ];
    //         }

    //     } 

    //     return new JsonResponse($data);
    // }

    // public function update($id, Request $request): JsonResponse
    // {
        
    //     $json = $request->getContent();
    //     $params = json_decode($json);

    //     if ($json != null) {
            
    //               // 'id' => $entity->getId(), 
    //               $comUnegos = (!empty($params->comUnegos)) ? $params->comUnegos : null;
    //               $comNrodeleg = (!empty($params->comNrodeleg)) ? $params->comNrodeleg : null;
    //               $comProceso = (!empty($params->comProceso)) ? $params->comProceso : null;
    //               $comNrocli = (!empty($params->comNrocli)) ? $params->comNrocli : null;
    //               $comNrocom = (!empty($params->comNrocom)) ? $params->comNrocom : null;
    //               $comPreimpreso = (!empty($params->comPreimpreso)) ? $params->comPreimpreso : null;
    //               $comNroope = (!empty($params->comNroope)) ? $params->comNroope : null;
    //               $comFecha= (!empty($params->comFecha)) ? $params->comFecha : null;
    //               $comTitulo = (!empty($params->comTitulo)) ? $params->comTitulo : null;
    //               $comMatricula = (!empty($params->comMatricula)) ? $params->comMatricula : null;
    //               $comSubcuenta = (!empty($params->comSubcuenta)) ? $params->comSubcuenta : null;
    //               $comTipdoc = (!empty($params->comTipdoc)) ? $params->comTipdoc : null;
    //               $comNrodoc = (!empty($params->comNrodoc)) ? $params->comNrodoc : null;
    //               $comTipcmt = (!empty($params->comTipcmt)) ? $params->comTipcmt : null;
    //               $comNrocmt = (!empty($params->comNrocmt)) ? $params->comNrocmt : null;
    //               $comNrocuo = (!empty($params->comNrocuo)) ? $params->comNrocuo : null;
    //               $comNropre = (!empty($params->comNropre)) ? $params->comNropre : null;
    //               $comTipo = (!empty($params->comTipo)) ? $params->comTipo : null;
    //               $comLote = (!empty($params->comLote)) ? $params->comLote : null;
    //               $comCaja = (!empty($params->comCaja)) ? $params->comCaja : null;
    //               $comNroreg = (!empty($params->comNroreg)) ? $params->comNroreg : null;
    //               $comZeta = (!empty($params->comZeta)) ? $params->comZeta : null;
    //               $comTotal = (!empty($params->comTotal)) ? $params->comTotal : null;
    //               $comTipanu = (!empty($params->comTipanu)) ? $params->comTipanu : null;
    //               $comPunanu = (!empty($params->comPunanu)) ? $params->comPunanu : null;
    //               $comNroanu = (!empty($params->comNroanu)) ? $params->comNroanu : null;
    //               $comFecalt = (!empty($params->comFecalt)) ? $params->comFecalt : null;
    //               $comEstado = (!empty($params->comEstado)) ? $params->comEstado : null;
    //               $comFecven = (!empty($params->comFecven)) ? $params->comFecven : null;
    //               $comFecpag = (!empty($params->comFecpag)) ? $params->comFecpag : null;
    //               $comImppag = (!empty($params->comImppag)) ? $params->comImppag : null;
    //               $comTipmov = (!empty($params->comTipmov)) ? $params->comTipmov : null;
    //               $comCoeficiente = (!empty($params->comCoeficiente)) ? $params->comCoeficiente : null;
    //               $comConcepto1 = (!empty($params->comConcepto1)) ? $params->comConcepto1 : null;
    //               $comConcepto2 = (!empty($params->comConcepto2)) ? $params->comConcepto2 : null;
    //               $comConcepto3 = (!empty($params->comConcepto3)) ? $params->comConcepto3 : null;
    //               $comLeyenda = (!empty($params->comLeyenda)) ? $params->comLeyenda : null;
    //               $comRefano = (!empty($params->comRefano)) ? $params->comRefano : null;
    //               $comRefmes = (!empty($params->comRefmes)) ? $params->comRefmes : null;
    //               $comNroasi = (!empty($params->comNroasi)) ? $params->comNroasi : null;
    //               $comTarea = (!empty($params->comTarea)) ? $params->comTarea : null;
    //               $comNrotrabajo = (!empty($params->comNrotrabajo)) ? $params->comNrotrabajo : null;
    //               $comAsigrupal = (!empty($params->comAsigrupal)) ? $params->comAsigrupal : null;
    //               $comNrolegali = (!empty($params->comNrolegali)) ? $params->comNrolegali : null;
    //               $comDestinatario = (!empty($params->comDestinatario)) ? $params->comDestinatario : null;
            
    //         if (
    //             !empty($comUnegos) &&
    //             !empty($comNrodeleg) &&
    //             !empty($comProceso) &&
    //             !empty($comNrocli) &&
    //             !empty($comNrocom) &&
    //             !empty($comPreimpreso) &&
    //             !empty($comNroope) &&
    //             !empty($comFecha) &&
    //             !empty($comTitulo) &&
    //             !empty($comMatricula) &&
    //             !empty($comSubcuenta) &&
    //             !empty($comTipdoc) &&
    //             !empty($comNrodoc) &&
    //             !empty($comTipcmt) &&
    //             !empty($comNrocmt) &&
    //             !empty($comNrocuo) &&
    //             !empty($comNropre) &&
    //             !empty($comTipo) &&
    //             !empty($comLote) &&
    //             !empty($comCaja) &&
    //             !empty($comNroreg) &&
    //             !empty($comZeta) &&
    //             !empty($comTotal) &&
    //             !empty($comTipanu) &&
    //             !empty($comPunanu) &&
    //             !empty($comNroanu) &&
    //             !empty($comFecalt) &&
    //             !empty($comEstado) &&
    //             !empty($comFecven) &&
    //             !empty($comFecpag) &&
    //             !empty($comImppag) &&
    //             !empty($comTipmov) &&
    //             !empty($comCoeficiente) && 
    //             !empty($comConcepto1) &&
    //             !empty($comConcepto2) &&
    //             !empty($comConcepto3) &&
    //             !empty($comLeyenda) &&
    //             !empty($comRefano) &&
    //             !empty($comRefmes) &&
    //             !empty($comAsiant) &&
    //             !empty($comTarea) &&
    //             !empty($comNrotrabajo) && 
    //             !empty($comAsigrupal) &&
    //             !empty($comNrolegali) &&
    //             !empty($comDestinatario)  
    //         ) {
                
    //             $updated_at = new \DateTime("now");

    //             $entity = $this->getDoctrine()->getRepository(Comproba::class)->findOneBy(['id' => $id]);
    //             $entity
    //             ->setComUnegos($comUnegos)
    //             ->setComNrodeleg($comNrodeleg)
    //             ->setComProceso($comProceso)
    //             ->setComNrocli($comNrocli)
    //             ->setComNrocom($comNrocom)
    //             ->setComPreimpreso($comPreimpreso)
    //             ->setComNroope($comNroope)
    //             ->setComFecha($comFecha)
    //             ->setComTitulo($comTitulo)
    //             ->setComMatricula($comMatricula)
    //             ->setComSubcuenta($comSubcuenta)
    //             ->setComTipdoc($comTipdoc)
    //             ->setComNrodoc($comNrodoc)
    //             ->setComTipcmt($comTipcmt)
    //             ->setComNrocmt($comNrocmt)
    //             ->setComNrocuo($comNrocuo)
    //             //->setPro_copias2($comNropre)
    //             ->setComNropre($comNropre)
    //             ->setComTipo($comTipo)
    //             ->setComLote($comLote)
    //             ->setComCaja($comCaja)
    //             ->setComNroreg($comNroreg)
    //             ->setComZeta($comZeta)
    //             ->setComTotal($comTotal)
    //             ->setComTipanu($comTipanu)
    //             ->setComPunanu($comPunanu)
    //             ->setComNroanu($comFecalt)
    //             ->setComFecalt($comEstado)
    //             ->setComEstado($comFecven)
    //             ->setComFecven($comFecpag)
    //             ->setComFecpag($comImppag)
    //             ->setComImppag($comImppag)
    //             ->setComTipmov($comTipmov)
    //             ->setComCoeficiente($comCoeficiente)
    //             ->setComNroasi($comNroasi)
    //             ->setComConcepto1($comConcepto1)
    //             ->setComConcepto2($comConcepto2)
    //             ->setComConcepto3($comConcepto3)
    //             ->setComLeyenda($comLeyenda)
    //             ->setComRefano($comRefano)
    //             ->setComRefmes($comRefmes)
    //             ->setComAsiant($comAsiant)
    //             ->setComTarea($comTarea)
    //             ->setComNrotrabajo($comNrotrabajo)
    //             ->setComAsigrupal($comAsigrupal)
    //             ->setComNrolegali($comNrolegali)
    //             ->setComDestinatario($comDestinatario)
    //             ;

    //             $em =  $this->getDoctrine()->getManager();
    //             $em->persist($entity);
    //             $em->flush();
                
    //             $data = [
    //                 'status' => 'success',
    //                 'code' => 200,
    //                 'message' => 'Elemento actualizado.'
    //             ];

    //         } else {
    //             $data = [
    //                 'status' => 'error',
    //                 'code' => 400,
    //                 'message' => 'El elemento no se ha podido actualizar.'
    //             ];
    //         }

    //     } 

    //     return new JsonResponse($data);
    // }
}