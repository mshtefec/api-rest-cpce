<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

class UserController extends AbstractController
{

    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(User::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'username' => $entity->getUsername(),
                'firstname' => $entity->getFirstname(),
                'lastname' => $entity->getLastname(),
                'email' => $entity->getEmail(),
                'role' => $entity->getRole(),
                'created_at' => $entity->getCreatedAt(),
                'updated_at' => $entity->getUpdatedAt()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id)
    {
        $entity = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

        $data = [
            'id' => $entity->getId(),
            'username' => $entity->getUsername(),
            'firstname' => $entity->getFirstname(),
            'lastname' => $entity->getLastname(),
            'email' => $entity->getEmail(),
            'role' => $entity->getRole(),
            'created_at' => $entity->getCreatedAt(),
            'updated_at' => $entity->getUpdatedAt()
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $username = (!empty($params->username)) ? $params->username : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $password = (!empty($params->password)) ? $params->password : null;
            $lastname = (!empty($params->lastname)) ? $params->lastname : null;
            $firstname = (!empty($params->firstname)) ? $params->firstname : null;
            $role = (!empty($params->role)) ? $params->role : null;
            
            $validator = Validation::createValidator();
            $validate_email = $validator->validate($email, [
                new Email()
            ]);

            if (
                !empty($username) && 
                !empty($email) && count($validate_email) == 0 && 
                !empty($password) &&
                !empty($lastname) &&
                !empty($firstname) &&
                !empty($role)
            ) {
                $created_at = new \DateTime("now");
                $updated_at = new \DateTime("now");

                $pwd = hash('sha256', $password);
                
                $entity = new User();
                $entity
                    ->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setUsername($username)
                    ->setEmail($email)
                    ->setRole($role)
                    ->setPassword($pwd)
                    ->setCreatedAt($created_at)
                    ->setUpdatedAt($updated_at)
                ;

                $exist_user = $this->getDoctrine()->getRepository(User::class)->findBy(
                    array(
                        'email' => $email
                    )
                );

                if (count($exist_user) == 0) {
                    $em =  $this->getDoctrine()->getManager();
                    $em->persist($entity);
                    $em->flush();
                
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Elemento creado.',
                        'User' => $entity
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ya existe ese usuario.'
                    ];   
                }

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $username = (!empty($params->username)) ? $params->username : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $password = (!empty($params->password)) ? $params->password : null;
            $lastname = (!empty($params->lastname)) ? $params->lastname : null;
            $firstname = (!empty($params->firstname)) ? $params->firstname : null;
            $role = (!empty($params->role)) ? $params->role : null;

            $validator = Validation::createValidator();
            $validate_email = $validator->validate($email, [
                new Email()
            ]);
            
            if (
                !empty($username) && 
                !empty($email) && count($validate_email) == 0 && 
                !empty($password) &&
                !empty($lastname) &&
                !empty($firstname) &&
                !empty($role)
            ) {
                $entity = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);
                
                $created_at = $entity->getCreatedAt();
                $updated_at = new \DateTime("now");

                $pwd = hash('sha256', $password);

                $entity
                    ->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setUsername($username)
                    ->setEmail($email)
                    ->setRole($role)
                    ->setPassword($pwd)
                    ->setCreatedAt($created_at)
                    ->setUpdatedAt($updated_at)
                ;

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function delete($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new JsonResponse(['status' => 'Element deleted!'], Response::HTTP_OK);
    }
}