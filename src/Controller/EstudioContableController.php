<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

use App\Entity\EstudioContable;
use App\Entity\Afiliado;
use App\Entity\EstudiocontablesAfiliados;

class EstudioContableController extends AbstractController
{
    public function getAll(Request $request, PaginatorInterface $paginator): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(EstudioContable::class)->findAll();
        $data = [];
        
        $data = [];
        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'denominacion' => $entity->getDenominacion(),
                'cuit' => $entity->getCuit(),
                'zona' => $entity->getZona(),
                'direccion' => $entity->getDireccion(),
                'localidad' => $entity->getLocalidad(),
                'provincia' => $entity->getProvincia(),
                'codigo_postal' => $entity->getCodigoPostal(),
                'email' => $entity->getEmail(),
                'telefono' => $entity->getTelefono(),
                'integrantes' => $entity->getIntegrantes()->toArray()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(EstudioContable::class)->findOneBy(['id' => $id]);

        $data = [
            'id' => $entity->getId(),
            'denominacion' => $entity->getDenominacion(),
            'cuit' => $entity->getCuit(),
            'zona' => $entity->getZona(),
            'direccion' => $entity->getDireccion(),
            'localidad' => $entity->getLocalidad(),
            'provincia' => $entity->getProvincia(),
            'codigo_postal' => $entity->getCodigoPostal(),
            'email' => $entity->getEmail(),
            'telefono' => $entity->getTelefono(),
            'integrantes' => $entity->getIntegrantes()->toArray()
        ];
        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {

            $denominacion = (!empty($params->denominacion)) ? $params->denominacion : null;
            $cuit = (!empty($params->cuit)) ? $params->cuit : null;
            $zona = (!empty($params->zona)) ? $params->zona : null;
            $direccion = (!empty($params->direccion)) ? $params->direccion : null;
            $localidad = (!empty($params->localidad)) ? $params->localidad : null;
            $provincia = (!empty($params->provincia)) ? $params->provincia : null;
            $codigo_postal = (!empty($params->codigo_postal)) ? $params->codigo_postal : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $telefono = (!empty($params->telefono)) ? $params->telefono : null;
            $integrantes = (!empty($params->integrantes)) ? $params->integrantes : '-';

            if (
                !empty($denominacion) &&
                !empty($cuit) &&
                !empty($zona) &&
                !empty($direccion) &&
                !empty($localidad) &&
                !empty($provincia) &&
                !empty($codigo_postal) &&
                !empty($email) &&
                !empty($telefono) &&
                !empty($integrantes) 
            ) {
                $entity = new EstudioContable();
                $entity 
                    ->setDenominacion($denominacion)
                    ->setCuit($cuit)
                    ->setZona($zona)
                    ->setDireccion($direccion)
                    ->setLocalidad($localidad)
                    ->setProvincia($provincia)
                    ->setCodigoPostal($codigo_postal)
                    ->setEmail($email)
                    ->setTelefono($telefono)
                ;

             
               $array = json_decode(json_encode($integrantes), true);
               if ($integrantes != '-') {
                   foreach ($array as $key => $integrante) {
                       $ecAfi = $this->getDoctrine()->getRepository(Afiliado::class)->findOneBy(['afiNrodoc' => $integrante['afi_nrodoc']]);
                       $entity->addIntegrante($ecAfi);
                   }
               }

                $exist = $this->getDoctrine()->getRepository(EstudioContable::class)->findOneBy(['cuit' => $cuit]);

                if (!$exist) {
                    $em =  $this->getDoctrine()->getManager();
                    $em->persist($entity);
                    $em->flush();
                
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Elemento creado.'
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ya existe el estudio contable con ese cuit.'
                    ];   
                }

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            $denominacion = (!empty($params->denominacion)) ? $params->denominacion : null;
            $cuit = (!empty($params->cuit)) ? $params->cuit : null;
            $zona = (!empty($params->zona)) ? $params->zona : null;
            $direccion = (!empty($params->direccion)) ? $params->direccion : null;
            $localidad = (!empty($params->localidad)) ? $params->localidad : null;
            $provincia = (!empty($params->provincia)) ? $params->provincia : null;
            $codigo_postal = (!empty($params->codigo_postal)) ? $params->codigo_postal : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $telefono = (!empty($params->telefono)) ? $params->telefono : null;
            $integrantes = (!empty($params->integrantes)) ? $params->integrantes : '-';

            if (
                !empty($denominacion) &&
                !empty($zona) &&
                !empty($direccion) &&
                !empty($localidad) &&
                !empty($provincia) &&
                !empty($codigo_postal) &&
                !empty($email) &&
                !empty($telefono)
            ) {
                $entity = $this->getDoctrine()->getRepository(EstudioContable::class)->findOneBy(['id' => $id]);
                $entity
                    ->setDenominacion($denominacion)
                    ->setZona($zona)
                    ->setDireccion($direccion)
                    ->setLocalidad($localidad)
                    ->setProvincia($provincia)
                    ->setCodigoPostal($codigo_postal)
                    ->setEmail($email)
                    ->setTelefono($telefono)
                ;
          
                $a = json_decode(json_encode($entity->getIntegrantes()->toArray()), true);
                foreach ($a as $key => $i) {
                    if(!empty($entity->getIntegrantes()->toArray())){
                        $ecAfi = $this->getDoctrine()->getRepository(Afiliado::class)->findOneBy(['afiNrodoc' => $i['afi_nrodoc']]);
                        $entity->removeIntegrante($ecAfi);
                    } 
                }

         
                 $array = json_decode(json_encode($integrantes), true);
                 if ($integrantes != '-') {
                     foreach ($array as $key => $integrante) {
                         $ecAfi = $this->getDoctrine()->getRepository(Afiliado::class)->findOneBy(['afiNrodoc' => $integrante['afi_nrodoc']]);
                         $entity->addIntegrante($ecAfi);
                     }
                 }

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }
    public function deleted($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(EstudioContable::class)->findOneBy(['id' => $id]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new JsonResponse(['status' => 'El Estudio Contable fue eliminado!'], Response::HTTP_OK);
    }

}