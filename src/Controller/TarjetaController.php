<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Tarjeta;

class TarjetaController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Tarjeta::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'numero' => $entity->getNumero(),
                'afiliado' => $entity->getAfiliado(),
                'nombre' => $entity->getNombre(),
                'fecha_vencimiento' => $entity->getFechaVencimiento(),
                'tipo' => $entity->getTipo(),
                'banco' => $entity->getBanco(),
                // 'franquicia' => $entity->getFranquicia(),
                // 'created_at' => $entity->getCreatedAt(),
                // 'updated_at' => $entity->getUpdatedAt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }
}