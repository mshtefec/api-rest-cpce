<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Procesos;

class ProcesosController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Procesos::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
               // 'id' => $entity->getId(), 
                'pto_codpro' => $entity->getPto_codpro(),
                'pro_nombre' => $entity->getPro_nombre(),
                'pro_impresion' => $entity->getPro_impresion(),
                'pro_operacion' => $entity->getPro_operacion(),
                'pro_numerador' => $entity->getPro_numerador(),
                'pro_leyenda1' => $entity->getPro_leyenda1(),
                'pro_leyenda2' => $entity->getPro_leyenda2(),
                'pro_leyenda3' => $entity->getPro_leyenda3(),
                'pro_leyenda4' => $entity->getPro_leyenda4(),
                'pro_cpto1' => $entity->getPro_cpto1(),
                'pro_cpto2' => $entity->getPro_cpto2(),
                'pro_cpto3' => $entity->getPro_cpto3(),
                'pro_copias' => $entity->getPro_copias(),
                'pro_buscaf' => $entity->getPro_buscaf(),
                'pro_cajachica' => $entity->getPro_cajachica(),
                'pro_instit' => $entity->getPro_instit(),
                'pro_copias2' => $entity->getPro_copias2(),
                'pro_procancela' => $entity->getPro_procancela(),
                'TIPO' => $entity->getTIPO(),
                'pro_bloqfech' => $entity->getPro_bloqfech(),
                'LEYENDA' => $entity->getLEYENDA(),
                'pro_catexcl' => $entity->getPro_catexcl(),
                'pro_reporte' => $entity->getPro_reporte(),
                // 'pro_reporte1' => $entity->getPro_reporte1(),
                // 'pro_reporte2' => $entity->getPro_reporte2(),
                // 'pro_reporte3' => $entity->getPro_reporte3(),
                // 'pro_reporte4' => $entity->getPro_reporte4(),
                'pro_destinatario' => $entity->getPro_destinatario(),
                // 'pro_activo' => $entity->getPro_activo(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Procesos::class)->findOneBy(['pto_codpro' => $id]);

        $data = [
                // 'id' => $entity->getId(), 
                'pro_codigo' => $entity->getProCodigo(),
                'pro_nombre' => $entity->getPro_nombre(),
                'pro_impresion' => $entity->getPro_impresion(),
                'pro_operacion' => $entity->getPro_operacion(),
                'pro_numerador' => $entity->getPro_numerador(),
                'pro_leyenda1' => $entity->getPro_leyenda1(),
                'pro_leyenda2' => $entity->getPro_leyenda2(),
                'pro_leyenda3' => $entity->getPro_leyenda3(),
                'pro_leyenda4' => $entity->getPro_leyenda4(),
                'pro_cpto1' => $entity->getPro_cpto1(),
                'pro_cpto2' => $entity->getPro_cpto2(),
                'pro_cpto3' => $entity->getPro_cpto3(),
                'pro_copias' => $entity->getPro_copias(),
                'pro_buscaf' => $entity->getPro_buscaf(),
                'pro_cajachica' => $entity->getPro_cajachica(),
                'pro_instit' => $entity->getPro_instit(),
                'pro_copias2' => $entity->getPro_copias2(),
                'pro_procancela' => $entity->getPro_procancela(),
                'TIPO' => $entity->getTIPO(),
                'pro_bloqfech' => $entity->getPro_bloqfech(),
                'LEYENDA' => $entity->getLEYENDA(),
                'pro_catexcl' => $entity->getPro_catexcl(),
                'pro_reporte' => $entity->getPro_reporte(),
                // 'pro_reporte1' => $entity->getPro_reporte1(),
                // 'pro_reporte2' => $entity->getPro_reporte2(),
                // 'pro_reporte3' => $entity->getPro_reporte3(),
                // 'pro_reporte4' => $entity->getPro_reporte4(),
                'pro_destinatario' => $entity->getPro_destinatario(),
                // 'pro_activo' => $entity->getPro_activo(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            
              // 'id' => $entity->getId(), 
            $proCodigo = (!empty($params->proCodigo)) ? $params->proCodigo : null;
            $proNombre = (!empty($params->proNombre)) ? $params->proNombre : null;
            $proImpresion = (!empty($params->proImpresion)) ? $params->proImpresion : null;
            $proOperacion = (!empty($params->proOperacion)) ? $params->proOperacion : null;
            $pro_numerador= (!empty($params->proNumerador)) ? $params->proNumerador : null;
            $proLeyenda1 = (!empty($params->proLeyenda1)) ? $params->proLeyenda1 : null;
            $proLeyenda2 = (!empty($params->proLeyenda2)) ? $params->proLeyenda2 : null;
            $proLeyenda3= (!empty($params->proLeyenda3)) ? $params->proLeyenda3 : null;
            $proLeyenda4 = (!empty($params->proLeyenda4)) ? $params->proLeyenda4 : null;
            $proCpto1 = (!empty($params->proCpto1)) ? $params->proCpto1 : null;
            $proCpto2 = (!empty($params->proCpto2)) ? $params->proCpto2 : null;
            $proCpto3 = (!empty($params->proCpto3)) ? $params->proCpto3 : null;
            $proCopias = (!empty($params->proCopias)) ? $params->proCopias : null;
            $proBuscaf = (!empty($params->proBuscaf)) ? $params->proBuscaf : null;
            $proCajachica = (!empty($params->proCajachica)) ? $params->proCajachica : null;
            $proInstit = (!empty($params->proInstit)) ? $params->proInstit : null;
            $proCopias2 = (!empty($params->proCopias2)) ? $params->proCopias2 : null;
            $proProcancela = (!empty($params->proProcancela)) ? $params->proProcancela : null;
            $tipo = (!empty($params->tipo)) ? $params->tipo : null;
            $proBloqfech = (!empty($params->proBloqfech)) ? $params->proBloqfech : null;
            $leyenda = (!empty($params->leyenda)) ? $params->leyenda : null;
            $proCatexcl = (!empty($params->proCatexcl)) ? $params->proCatexcl : null;
            $proReporte = (!empty($params->proReporte)) ? $params->proReporte : null;
            // $proReporte1 = (!empty($params->proReporte1)) ? $params->proReporte1 : null;
            // $proReporte2 = (!empty($params->proReporte2)) ? $params->proReporte2 : null;
            // $proReporte3 = (!empty($params->proReporte3)) ? $params->proReporte3 : null;
            // $proReporte4 = (!empty($params->proReporte4)) ? $params->proReporte4 : null;
            $proDestinatario = (!empty($params->proDestinatario)) ? $params->proDestinatario : null;
            // $proActivo = (!empty($params->proActivo)) ? $params->proActivo : null;

            
            if (
                !empty($proCodpro) &&
                !empty($proNombre) &&
                !empty($proImpresion) &&
                !empty($proOperacion) &&
                !empty($proNumerador) &&
                !empty($proLeyenda1) &&
                !empty($proLeyenda2) &&
                !empty($proLeyenda3) &&
                !empty($proLeyenda4) &&
                !empty($proCpto1) &&
                !empty($proCpto2) &&
                !empty($proCpto3) &&
                !empty($proCopias) &&
                !empty($proBuscaf) &&
                !empty($proCajachica) &&
                !empty($proInstit) &&
                !empty($proCopias2) &&
                !empty($proProcancela) &&
                !empty($tipo) &&
                !empty($proBloqfech) &&
                !empty($leyenda) &&
                !empty($proCatexcl) &&
                !empty($proReporte) &&
                // !empty($proReporte1) &&
                // !empty($proReporte2) &&
                // !empty($proReporte3) &&
                // !empty($proReporte4) &&
                !empty($proDestinatario) 
                // !empty($proActivo) 

            ) {
                $created_at = new \DateTime("now");
                $updated_at = new \DateTime("now");

                $entity = new Procesos();
                $entity
                  
                    //->setProCodpro($proCodpro) 
                    ->setProNombre($proNombre) 
                    ->setProImpresion($proImpresion)
                    ->setProOperacion($proOperacion)
                    ->setProNumerador($proNumerador)
                    ->setProLeyenda1($proLeyenda1) 
                    ->setProLeyenda2($proLeyenda2) 
                    ->setProLeyenda3($proLeyenda3) 
                    ->setProLeyenda4($proLeyenda4) 
                    ->setProCpto1($proCpto1) 
                    ->setProCpto2($proCpto2) 
                    ->setProCpto3($proCpto3) 
                    ->setProCopias($proCopias) 
                    ->setProBuscaf($proBuscaf) 
                    ->setProCajachica($proCajachica) 
                    ->setProInstit($proInstit) 
                    ->setPRoCopias2($proCopias2) 
                    ->setProProcancela($proProcancela) 
                    ->setTipo($tipo) 
                    ->setProBloqfech($proBloqfech) 
                    ->setLeyenda($leyenda) 
                    ->setProCatexcl($proCatexcl) 
                    ->setProReporte($proReporte) 
                    // ->setProReporte1($proReporte1) 
                    // ->setProReporte2($proReporte2) 
                    // ->setProReporte3($proReporte3) 
                    // ->setProReporte4($proReporte4) 
                    ->setProDestinatario($proDestinatario) 
                    //->setProActivo($proActivo) 
                    ;

                $em =  $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento creado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $proCodigo = (!empty($params->proCodigo)) ? $params->proCodigo : null;
            $proNombre = (!empty($params->proNombre)) ? $params->proNombre : null;
            $proImpresion = (!empty($params->proImpresion)) ? $params->proImpresion : null;
            $proOperacion = (!empty($params->proOperacion)) ? $params->proOperacion : null;
            $pro_numerador= (!empty($params->proNumerador)) ? $params->proNumerador : null;
            $proLeyenda1 = (!empty($params->proLeyenda1)) ? $params->proLeyenda1 : null;
            $proLeyenda2 = (!empty($params->proLeyenda2)) ? $params->proLeyenda2 : null;
            $proLeyenda3= (!empty($params->proLeyenda3)) ? $params->proLeyenda3 : null;
            $proLeyenda4 = (!empty($params->proLeyenda4)) ? $params->proLeyenda4 : null;
            $proCpto1 = (!empty($params->proCpto1)) ? $params->proCpto1 : null;
            $proCpto2 = (!empty($params->proCpto2)) ? $params->proCpto2 : null;
            $proCpto3 = (!empty($params->proCpto3)) ? $params->proCpto3 : null;
            $proCopias = (!empty($params->proCopias)) ? $params->proCopias : null;
            $proBuscaf = (!empty($params->proBuscaf)) ? $params->proBuscaf : null;
            $proCajachica = (!empty($params->proCajachica)) ? $params->proCajachica : null;
            $proInstit = (!empty($params->proInstit)) ? $params->proInstit : null;
            $proCopias2 = (!empty($params->proCopias2)) ? $params->proCopias2 : null;
            $proProcancela = (!empty($params->proProcancela)) ? $params->proProcancela : null;
            $tipo = (!empty($params->tipo)) ? $params->tipo : null;
            $proBloqfech = (!empty($params->proBloqfech)) ? $params->proBloqfech : null;
            $leyenda = (!empty($params->leyenda)) ? $params->leyenda : null;
            $proCatexcl = (!empty($params->proCatexcl)) ? $params->proCatexcl : null;
            $proReporte = (!empty($params->proReporte)) ? $params->proReporte : null;
            // $proReporte1 = (!empty($params->proReporte1)) ? $params->proReporte1 : null;
            // $proReporte2 = (!empty($params->proReporte2)) ? $params->proReporte2 : null;
            // $proReporte3 = (!empty($params->proReporte3)) ? $params->proReporte3 : null;
            // $proReporte4 = (!empty($params->proReporte4)) ? $params->proReporte4 : null;
            $proDestinatario = (!empty($params->proDestinatario)) ? $params->proDestinatario : null;
            // $proActivo = (!empty($params->proActivo)) ? $params->proActivo : null;

            
            if (
                !empty($proCodpro) &&
                !empty($proNombre) &&
                !empty($proImpresion) &&
                !empty($proOperacion) &&
                !empty($proNumerador) &&
                !empty($proLeyenda1) &&
                !empty($proLeyenda2) &&
                !empty($proLeyenda3) &&
                !empty($proLeyenda4) &&
                !empty($proCpto1) &&
                !empty($proCpto2) &&
                !empty($proCpto3) &&
                !empty($proCopias) &&
                !empty($proBuscaf) &&
                !empty($proCajachica) &&
                !empty($proInstit) &&
                !empty($proCopias2) &&
                !empty($proProcancela) &&
                !empty($tipo) &&
                !empty($proBloqfech) &&
                !empty($leyenda) &&
                !empty($proCatexcl) &&
                !empty($proReporte) &&
                // !empty($proReporte1) &&
                // !empty($proReporte2) &&
                // !empty($proReporte3) &&
                // !empty($proReporte4) &&
                !empty($proDestinatario) 
                // !empty($proActivo) 
            ) {
                
                $updated_at = new \DateTime("now");

                $entity = $this->getDoctrine()->getRepository(Procesos::class)->findOneBy(['id' => $id]);
                $entity
                ->setPtoCodpro($proCodpro) 
                ->setPtoNombre($proNombre) 
                ->setPtoImpresion($proImpresion)
                ->setPtoOperacion($proOperacion)
                ->setPtoNumerador($proNumerador)
                ->setPtoLeyenda1($proLeyenda1) 
                ->setPtoLeyenda2($proLeyenda2) 
                ->setPtoLeyenda3($proLeyenda3) 
                ->setPtoLeyenda4($proLeyenda4) 
                ->setPto_($proCpto1) 
                ->setPto_($proCpto2) 
                ->setPto_($proCpto3) 
                ->setPto_($proCopias) 
                ->setPto_($proBuscaf) 
                ->setPto_($proCajachica) 
                ->setPto_($proInstit) 
                ->setPto_($proCopias2) 
                ->setPto_($proProcancela) 
                ->setPto_($tipo) 
                ->setPto_($proBloqfech) 
                ->setPto_($leyenda) 
                ->setPto_($proCatexcl) 
                ->setPto_($proReporte) 
                // ->setPto_($proReporte1) 
                // ->setPto_($proReporte2) 
                // ->setPto_($proReporte3) 
                // ->setPto_($proReporte4) 
                ->setPto_($proDestinatario) 
                // ->setPto_($proActivo) 
                ;

                $em =  $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }
}