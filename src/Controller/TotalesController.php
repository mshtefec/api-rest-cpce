<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Totales;

class TotalesController extends AbstractController
{

    public function getAll(Request $request, PaginatorInterface $paginator): JsonResponse
    {
        //$entities = $this->getDoctrine()->getRepository(Totales::class)->findAll();
        $page = $request->query->getInt('page', 1);
        $item_per_page = 100;

        $dql = "
            SELECT t
            FROM App\Entity\Totales t
            ORDER BY t.totNroasi DESC 
        ";

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate($query, $page, $item_per_page, array('wrap-queries' => true, 'distinct' => false));
        $total = $pagination->getTotalItemCount();
        
        $data = [];
        foreach ($pagination as $entity) {
            $data[] = [
                'tot_unegos' => $entity->getTotUnegos(),
                'tot_nrocli' => $entity->getTotNrocli(),
                'tot_item' => $entity->getTotItem(),
                'tot_nrocuo' => $entity->getTotNrocuo(),
                'tot_nroasi' => $entity->getTotNroasi(),
                'tot_nrocom' => $entity->getTotNrocom(),
                'tot_proceso' => $entity->getTotProceso(),
                'tot_nroope' => $entity->getTotNroope(),
                'tot_tipdoc' => $entity->getTotTipdoc(),
                'tot_nrodoc' => $entity->getTotNrodoc(),
                'tot_titulo' => $entity->getTotTitulo(),
                'tot_matricula' => $entity->getTotMatricula(),
                'tot_zeta' => $entity->getTotZeta(),
                // 'tot_nropla' => $entity->getTotNropla(),
                'tot_debe' => $entity->getTotDebe(),
                'tot_haber' => $entity->getTotHaber(),
                'tot_fecha' => $entity->getTotFecha(),
                'tot_fecalt' => $entity->getTotFecalt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getTotCheques(): JsonResponse
    {
        $now = new \DateTime();
        $tomorrow = new \DateTime();
        $tomorrow = $tomorrow->add(new \DateInterval('P1D'));
        $yesterday = new \DateTime();
        $yesterday = $yesterday->sub(new \DateInterval('P1D'));

        $dql = "
            SELECT 
                t
            FROM 
                App\Entity\Totales t
            WHERE
                t.totProceso = '02R120' AND
                t.totNropla = 11010102 AND
                t.totEstado <> 9 AND
                t.totEstche <> 'C' AND
                t.totFecdif > '" . $yesterday->format('Y-m-d') . "' AND
                t.totFecdif < '" . $tomorrow->format('Y-m-d') .  "'
            ORDER BY 
                t.totFecdif DESC,
                t.totNroasi DESC
        ";

        // t.totFecalt <= '2021-31-12' AND 2021-03-08'
        // t.totFecalt >= '2020-01-01' 2021-03-10

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql);

        $entities = $query->getResult();
        
        $data = [];
        foreach ($entities as $entity) {
            $data[] = [
                'tot_unegos' => $entity->getTotUnegos(),
                'tot_proceso' => $entity->getTotProceso(),
                'tot_nrocli' => $entity->getTotNrocli(),
                'tot_nrocom' => $entity->getTotNrocom(),
                'tot_item' => $entity->getTotItem(),
                'tot_nrocuo' => $entity->getTotNrocuo(),
                'tot_fecha' => $entity->getTotFecha(),
                'tot_fecven' => $entity->getTotFecven(),
                'tot_debe' => $entity->getTotDebe(),
                'tot_haber' => $entity->getTotHaber(),
                'tot_nropla' => $entity->getTotNropla(),
                'tot_subpla' => $entity->getTotSubpla(),
                'tot_tipdoc' => $entity->getTotTipdoc(),
                'tot_nrodoc' => $entity->getTotNrodoc(),
                'tot_titulo' => $entity->getTotTitulo(),
                'tot_matricula' => $entity->getTotMatricula(),
                'tot_nroope' => $entity->getTotNroope(),
                'tot_zeta' => $entity->getTotZeta(),
                'tot_nroasi' => $entity->getTotNroasi(),
                'tot_nrolegali' => $entity->getTotNrolegali(),
                'tot_concepto' => $entity->getTotConcepto(),
                'tot_nrocheque' => $entity->getTotNrocheque(),
                'tot_fecche' => $entity->getTotFecche(),
                'tot_fecdif' => $entity->getTotFecdif(),
                'tot_tipdes' => $entity->getTotTipdes(),
                'tot_fecalt' => $entity->getTotFecalt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Totales::class)->findOneBy(['totNroasi' => $id]);

        $data = [
            'tot_unegos' => $entity->getTotUnegos(),
            'tot_nrocli' => $entity->getTotNrocli(),
            'tot_item' => $entity->getTotItem(),
            'tot_nrocuo' => $entity->getTotNrocuo(),
            'tot_nroasi' => $entity->getTotNroasi(),
            'tot_nrocom' => $entity->getTotNrocom(),
            'tot_proceso' => $entity->getTotProceso(),
            'tot_nroope' => $entity->getTotNroope(),
            'tot_tipdoc' => $entity->getTotTipdoc(),
            'tot_nrodoc' => $entity->getTotNrodoc(),
            'tot_titulo' => $entity->getTotTitulo(),
            'tot_matricula' => $entity->getTotMatricula(),
            'tot_zeta' => $entity->getTotZeta(),
            // 'tot_nropla' => $entity->getTotNropla(),
            'tot_debe' => $entity->getTotDebe(),
            'tot_haber' => $entity->getTotHaber(),
            'tot_fecha' => $entity->getTotFecha(),
            'tot_fecalt' => $entity->getTotFecalt(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    // public function add(Request $request): JsonResponse
    // {
        
    //     $json = $request->getContent();
    //     $params = json_decode($json);

    //     if ($json != null) {
            
    //         $name = (!empty($params->name)) ? $params->name : null;
    //         $description = (!empty($params->description)) ? $params->description : null;
    //         $stock = (!empty($params->stock)) ? $params->stock : null;
    //         $price = (!empty($params->price)) ? $params->price : null;
            
    //         if (
    //             !empty($name) && 
    //             !empty($description) &&
    //             !empty($stock) &&
    //             !empty($price) 
    //         ) {
    //             $created_at = new \DateTime("now");
    //             $updated_at = new \DateTime("now");

    //             $entity = new Supply();
    //             $entity
    //                 ->setName($name)
    //                 ->setDescription($description)
    //                 ->setStock($stock)
    //                 ->setPrice($price)
    //                 ->setCreatedAt($created_at)
    //                 ->setUpdatedAt($updated_at)
    //             ;

    //             $em =  $this->getDoctrine()->getManager();
    //             $em->persist($entity);
    //             $em->flush();
                
    //             $data = [
    //                 'status' => 'success',
    //                 'code' => 200,
    //                 'message' => 'Elemento creado.'
    //             ];

    //         } else {
    //             $data = [
    //                 'status' => 'error',
    //                 'code' => 400,
    //                 'message' => 'El elemento no se ha podido crear.'
    //             ];
    //         }

    //     } 

    //     return new JsonResponse($data);
    // }

    // public function update($id, Request $request): JsonResponse
    // {
        
    //     $json = $request->getContent();
    //     $params = json_decode($json);

    //     if ($json != null) {
            
    //         $name = (!empty($params->name)) ? $params->name : null;
    //         $description = (!empty($params->description)) ? $params->description : null;
    //         $stock = (!empty($params->stock)) ? $params->stock : null;
    //         $price = (!empty($params->price)) ? $params->price : null;
            
    //         if (
    //             !empty($name) && 
    //             !empty($description) &&
    //             !empty($stock) &&
    //             !empty($price) 
    //         ) {
                
    //             $updated_at = new \DateTime("now");

    //             $entity = $this->getDoctrine()->getRepository(Supply::class)->findOneBy(['id' => $id]);
    //             $entity
    //                 ->setName($name)
    //                 ->setDescription($description)
    //                 ->setStock($stock)
    //                 ->setPrice($price)
    //                 ->setUpdatedAt($updated_at)
    //             ;

    //             $em =  $this->getDoctrine()->getManager();
    //             $em->persist($entity);
    //             $em->flush();
                
    //             $data = [
    //                 'status' => 'success',
    //                 'code' => 200,
    //                 'message' => 'Elemento actualizado.'
    //             ];

    //         } else {
    //             $data = [
    //                 'status' => 'error',
    //                 'code' => 400,
    //                 'message' => 'El elemento no se ha podido actualizar.'
    //             ];
    //         }

    //     } 

    //     return new JsonResponse($data);
    // }
}