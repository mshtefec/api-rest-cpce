<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\AdhesionesDebito;
use App\Entity\Afiliado;
use App\Entity\Tarjeta;
use App\Entity\Plancuen;

class AdhesionesDebitoController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(AdhesionesDebito::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'afiliado' => $entity->getAfiliado(),
                'tarjeta' => $entity->getTarjeta(),
                'fecha' => $entity->getFecha(),
                'cuenta' => $entity->getCuenta(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(AdhesionesDebito::class)->findOneBy(['id' => $id]);

        $data = [
            'id' => $entity->getId(),
            'afiliado' => $entity->getAfiliado(),
            'tarjeta' => $entity->getTarjeta(),
            'fecha' => $entity->getFecha(),
            'cuenta' => $entity->getCuenta(),
        ];
        
        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {

            $afiliado = (!empty($params->afiliado)) ? $params->afiliado : null;
            $tarjeta = (!empty($params->tarjeta)) ? $params->tarjeta : null;
            $cuenta = (!empty($params->cuenta)) ? $params->cuenta : null;

            if (
                !empty($afiliado) &&
                !empty($tarjeta) &&
                !empty($cuenta)
            ) {
                
                $afiliado_db = $this->getDoctrine()->getRepository(Afiliado::class)->find(['afiNrodoc' => $afiliado->afi_nrodoc]);
                $tarjeta_db = $this->getDoctrine()->getRepository(Tarjeta::class)->find($tarjeta->numero);
                $cuenta_db = $this->getDoctrine()->getRepository(Plancuen::class)->find(['plaNropla' => $cuenta->code]);
                $created_at = new \DateTime("now");
                
                $entity = new AdhesionesDebito();
                $entity 
                    ->setAfiliado($afiliado_db)
                    ->setTarjeta($tarjeta_db)
                    ->setCuenta($cuenta_db)
                    ->setTipo("Tarjeta")
                    ->setImporte(0.00)
                    ->setFecha($created_at)
                    ->setMeses(12)
                    ->setEstado("ACTIVO")
                ;

                $exist = $this->getDoctrine()->getRepository(AdhesionesDebito::class)->findOneBy(
                    [
                        'afiliado' => $afiliado_db,
                        'cuenta' => $cuenta_db,
                        'estado' => 'ACTIVO',
                    ]
                );

                if (!$exist) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($entity);
                    $em->flush();
                
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Elemento creado.'
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ya existe esta adhesion con ese cuit.'
                    ];   
                }

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function deleted($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(AdhesionesDebito::class)->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new JsonResponse(['status' => 'La adhesión fue eliminada!'], Response::HTTP_OK);
    }
}