<?php
namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

use App\Entity\Familiar;
use App\Entity\ObraSocial;
use App\Entity\Beneficio;

class FamiliarController extends AbstractController
{
    public function getAll(Request $request, PaginatorInterface $paginator): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Familiar::class)->findAll();
        $data = [];
        
        $data = [];
        foreach ($entities as $entity) {
            $data[] = [
                'dni' => $entity->getDni(),
                'apellido' => $entity->getApellido(),
                'nombre' => $entity->getNombre(),
                'fecha_nacimiento' => $entity->getFechaNacimiento(),
                'direccion' => $entity->getDireccion(),
                'localidad' => $entity->getLocalidad(),
                'provincia' => $entity->getProvincia(),
                'codigo_postal' => $entity->getCodigoPostal(),
                'telefono' => $entity->getTelefono(),
                'mail' => $entity->getMail(),
                'estado_civil' => $entity->getEstadoCivil(),
                'sexo' => $entity->getSexo(),
                'cbu' => $entity->getCbu(),
                'obraSocial' => $entity->getObraSocial(),
                'cuit' => $entity->getCuit(),
                'parentesco' => $entity->getParentesco(),
                'afiliado' => $entity->getAfiliado(),
                'id' => $entity->getId(),
                'beneficios' => $entity->getBeneficios()->toArray(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Familiar::class)->findOneBy(['dni' => $id]);

        $data = [
            'dni' => $entity->getDni(),
            'apellido' => $entity->getApellido(),
            'nombre' => $entity->getNombre(),
            'fecha_nacimiento' => $entity->getFechaNacimiento(),
            'direccion' => $entity->getDireccion(),
            'localidad' => $entity->getLocalidad(),
            'provincia' => $entity->getProvincia(),
            'codigo_postal' => $entity->getCodigoPostal(),
            'telefono' => $entity->getTelefono(),
            'mail' => $entity->getMail(),
            'estado_civil' => $entity->getEstadoCivil(),
            'sexo' => $entity->getSexo(),
            'cbu' => $entity->getCbu(),
            'obraSocial' => $entity->getObraSocial(),
            'cuit' => $entity->getCuit(),
            'parentesco' => $entity->getParentesco(),
            'afiliado' => $entity->getAfiliado(),
            'id' => $entity->getId(),
            'beneficios' => $entity->getBeneficios()->toArray(),
        ];
        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {

            $afi_id = (!empty($params->afi_id)) ? $params->afi_id : null;
            $dni = (!empty($params->dni)) ? $params->dni : null;
            $cuit = (!empty($params->cuit)) ? $params->cuit : null;
            $parentesco = (!empty($params->parentesco)) ? $params->parentesco : null;
            $apellido = (!empty($params->apellido)) ? $params->apellido : null;
            $nombre = (!empty($params->nombre)) ? $params->nombre : null;
            $fecha_nacimiento = (!empty($params->fecha_nacimiento)) ? $params->fecha_nacimiento : null;
            $direccion = (!empty($params->direccion)) ? $params->direccion : null;
            $localidad = (!empty($params->localidad)) ? $params->localidad : null;
            $provincia = (!empty($params->provincia)) ? $params->provincia : null;
            $codigo_postal = (!empty($params->codigo_postal)) ? $params->codigo_postal : null;
            $telefono = (!empty($params->telefono)) ? $params->telefono : 0;
            $mail = (!empty($params->mail)) ? $params->mail : null;
            $estado_civil = (!empty($params->estado_civil)) ? $params->estado_civil : null;
            $sexo = (!empty($params->sexo)) ? $params->sexo : null;
            $cbu = (!empty($params->cbu)) ? $params->cbu : 0;
            $obraSocial = (!empty($params->obraSocial)) ? $params->obraSocial : '-';
            $beneficio_fondosolidario = (!empty($params->beneficio_fondosolidario)) ? $params->beneficio_fondosolidario : false;
            $beneficio_segurodevida = (!empty($params->beneficio_segurodevida)) ? $params->beneficio_segurodevida : false;
            $beneficio_capitalizacion = (!empty($params->beneficio_capitalizacion)) ? $params->beneficio_capitalizacion : false;
            $beneficios = (!empty($params->beneficios)) ? $params->beneficios : null;
            if (
                !empty($afi_id) &&
                !empty($cuit) &&
                !empty($parentesco) &&
                !empty($dni) &&
                !empty($apellido) &&
                !empty($nombre) &&
                !empty($fecha_nacimiento) &&
                !empty($direccion) &&
                !empty($localidad) &&
                !empty($provincia) &&
                !empty($codigo_postal) &&
                !empty($mail) &&
                !empty($estado_civil) &&
                !empty($sexo) &&
                !empty($obraSocial)
            ) {
                $temObraSocial = $this->getDoctrine()->getRepository(ObraSocial::class)->findOneBy(['id' => $obraSocial]);

                $temp_afi_fecnac = new \DateTime($fecha_nacimiento);
                $entity = new Familiar();
                $entity 
                    ->setAfiId($afi_id)
                    ->setCuit($cuit)
                    ->setDni($dni)
                    ->setApellido($apellido)
                    ->setParentesco($parentesco)
                    ->setNombre($nombre)
                    ->setFechaNacimiento($temp_afi_fecnac)
                    ->setDireccion($direccion)
                    ->setLocalidad($localidad)
                    ->setProvincia($provincia)
                    ->setCodigoPostal($codigo_postal)
                    ->setTelefono($telefono)
                    ->setMail($mail)
                    ->setEstadoCivil($estado_civil)
                    ->setSexo($sexo)
                    ->setCbu($cbu)
                    ->setObraSocial($temObraSocial)
                    ->setBeneficioFondoSolidario($beneficio_fondosolidario)
                    ->setBeneficioSeguroDeVida($beneficio_segurodevida)
                    ->setBeneficioCapitalizacion($beneficio_capitalizacion)
                ;
                
                $array = json_decode(json_encode($beneficios), true);
                if(!empty($array)){
                    foreach ($array as $key => $i) {
                        $temBenef = $this->getDoctrine()->getRepository(Beneficio::class)->findOneBy(['nombre' => $i['nombre']]);
                        $entity->addBeneficio($temBenef);
                    } 
                }  

                $exist = $this->getDoctrine()->getRepository(Familiar::class)->findOneBy(['dni' => $dni]);

                if (!$exist) {
                    $em =  $this->getDoctrine()->getManager();
                    $em->persist($entity);
                    $em->flush();
                
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Elemento creado.'
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ya existe ese usuario.'
                    ];   
                }

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {

            $id = (!empty($params->id)) ? $params->id : null;
            $dni = (!empty($params->dni)) ? $params->dni : null;
            $afiId = (!empty($params->afiId)) ? $params->afiId : null;
            $cuit = (!empty($params->cuit)) ? $params->cuit : null;
            $parentesco = (!empty($params->parentesco)) ? $params->parentesco : null;
            $apellido = (!empty($params->apellido)) ? $params->apellido : null;
            $nombre = (!empty($params->nombre)) ? $params->nombre : null;
            $fecha_nacimiento = (!empty($params->fecha_nacimiento)) ? $params->fecha_nacimiento : null;
            $direccion = (!empty($params->direccion)) ? $params->direccion : null;
            $localidad = (!empty($params->localidad)) ? $params->localidad : null;
            $provincia = (!empty($params->provincia)) ? $params->provincia : null;
            $codigo_postal = (!empty($params->codigo_postal)) ? $params->codigo_postal : null;
            $telefono = (!empty($params->telefono)) ? $params->telefono : null;
            $mail = (!empty($params->mail)) ? $params->mail : null;
            $estado_civil = (!empty($params->estado_civil)) ? $params->estado_civil : null;
            $sexo = (!empty($params->sexo)) ? $params->sexo : null;
            $cbu = (!empty($params->cbu)) ? $params->cbu : '-';
            $obraSocial = (!empty($params->obraSocial)) ? $params->obraSocial : null;
            $beneficio_fondosolidario = (!empty($params->beneficio_fondosolidario)) ? $params->beneficio_fondosolidario : false;
            $beneficio_segurodevida = (!empty($params->beneficio_segurodevida)) ? $params->beneficio_segurodevida : false;
            $beneficio_capitalizacion = (!empty($params->beneficio_capitalizacion)) ? $params->beneficio_capitalizacion : false;
            $beneficios = (!empty($params->beneficios)) ? $params->beneficios : null;
            if (
                
                !empty($parentesco) &&
                !empty($cuit) &&
                !empty($dni) &&
                !empty($apellido) &&
                !empty($nombre) &&
                !empty($fecha_nacimiento) &&
                !empty($direccion) &&
                !empty($localidad) &&
                !empty($provincia) &&
                !empty($codigo_postal) &&
                !empty($telefono) &&
                !empty($mail) &&
                !empty($estado_civil) &&
                !empty($sexo) &&
                !empty($cbu) &&
                !empty($obraSocial)
            ) {
                
                $temp_afi_fecnac = new \DateTime($fecha_nacimiento);
                $entity = $this->getDoctrine()->getRepository(Familiar::class)->findOneBy(['id' => $id]);
                $temObraSocial = $this->getDoctrine()->getRepository(ObraSocial::class)->findOneBy(['id' => $obraSocial]);
                $entity
                    ->setDni($dni)
                    ->setParentesco($parentesco)
                    ->setCuit($cuit)
                    ->setApellido($apellido)
                    ->setNombre($nombre)
                    ->setFechaNacimiento($temp_afi_fecnac)
                    ->setDireccion($direccion)
                    ->setLocalidad($localidad)
                    ->setProvincia($provincia)
                    ->setCodigoPostal($codigo_postal)
                    ->setTelefono($telefono)
                    ->setMail($mail)
                    ->setEstadoCivil($estado_civil)
                    ->setSexo($sexo)
                    ->setCbu($cbu)
                    ->setObraSocial($temObraSocial)
                    ->setBeneficioFondoSolidario($beneficio_fondosolidario)
                    ->setBeneficioSeguroDeVida($beneficio_segurodevida)
                    ->setBeneficioCapitalizacion($beneficio_capitalizacion)
                ;
                
                if(!empty($entity->getBeneficios()->toArray())){
                    foreach ($entity->getBeneficios()->toArray() as $key => $i) {
                        $temBenef = $this->getDoctrine()->getRepository(Beneficio::class)->findOneBy(['nombre' => $i->getNombre()]);
                        $entity->removeBeneficio($temBenef);
                    }
                } 
             
                $array = json_decode(json_encode($beneficios), true);
                if(!empty($array)){
                    foreach ($array as $key => $i) {
                        $temBenef = $this->getDoctrine()->getRepository(Beneficio::class)->findOneBy(['nombre' => $i['nombre']]);
                        $entity->addBeneficio($temBenef);
                    } 
                }  

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }
    public function deleted($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Familiar::class)->findOneBy(['id' => $id]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new JsonResponse(['status' => 'El familiar fue eliminado!'], Response::HTTP_OK);
    }

}