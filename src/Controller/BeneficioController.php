<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

use App\Entity\Beneficio;

class BeneficioController extends AbstractController
{
    public function getAll(Request $request, PaginatorInterface $paginator): JsonResponse
    {
        $page = $request->query->getInt('page', 1);
        $item_per_page = 100;

        $dql = "
            SELECT a
            FROM App\Entity\Beneficio a
            ORDER BY a.id  
        ";

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate($query, $page, $item_per_page, array('wrap-queries' => true, 'distinct' => false));
        $total = $pagination->getTotalItemCount();
        
        $data = [];
        foreach ($pagination as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'nombre' => $entity->getNombre(),
                'activo' => $entity->getActivo(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }
 
}