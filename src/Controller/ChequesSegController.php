<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\ChequeEstado;
use App\Entity\Totales;

class ChequesSegController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(ChequeEstado::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(), 
                'nro' => $entity->getNro(),
                'asiento' => $entity->getAsiento(),
                'legalizacion' => $entity->getLegalizacion(),
                'importe' => $entity->getImporte(),
                'banco' => $entity->getBanco(),
                'fec_diferida' => $entity->getFecDiferida(),
                'fec_comprobante' => $entity->getFecComprobante(),
                'estado' => $entity->getEstado(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function update($id, Request $request): JsonResponse
    {
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {

            $nro = (!empty($params->nro)) ? $params->nro : null;
            $asiento = (!empty($params->asiento)) ? $params->asiento : null;
            $legalizacion = (!empty($params->legalizacion)) ? $params->legalizacion : null;
            $importe = (!empty($params->importe)) ? $params->importe : null;
            $banco = (!empty($params->banco)) ? $params->banco : null;
            $fec_diferida = (!empty($params->fec_diferida)) ? $params->fec_diferida->date : null;
            $fec_comprobante = (!empty($params->fec_comprobante)) ? $params->fec_comprobante->date : null;
            $estado = (!empty($params->estado)) ? $params->estado : null;

            if (
                !empty($nro) &&
                !empty($asiento) &&
                !empty($legalizacion) &&
                !empty($importe) &&
                !empty($banco) &&
                !empty($fec_diferida) &&
                !empty($fec_comprobante) &&
                !empty($estado)
            ) {
                $temp_fec_diferida = new \DateTime($fec_diferida);
                $temp_fec_comprobante = new \DateTime($fec_comprobante);
                
                $entity = $this->getDoctrine()->getRepository(ChequeEstado::class)->findOneBy(['nro' => $id]);

                $em = $this->getDoctrine()->getManager();
                
                if (!$entity) {
                    $entity = new ChequeEstado();
                    $entity->setNro($nro);

                    $totalesRow = $this->getDoctrine()->getRepository(Totales::class)->findOneBy([
                        'totNroasi' => $asiento,
                        'totProceso' => '02R120',
                        'totNropla' => '11010102',
                        'totNrocheque' => $id
                    ]);

                    $totalesRow->setTotestche('C');
                    $em->persist($totalesRow);
                }

                $entity
                    ->setAsiento($asiento)
                    ->setLegalizacion($legalizacion)
                    ->setImporte($importe)
                    ->setBanco($banco)
                    ->setFecDiferida($temp_fec_diferida)
                    ->setFecComprobante($temp_fec_comprobante)
                    ->setEstado($estado)
                ;

                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }
}