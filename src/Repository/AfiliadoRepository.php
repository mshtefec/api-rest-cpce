<?php

namespace App\Repository;

use App\Entity\Afiliado;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Afiliado|null find($id, $lockMode = null, $lockVersion = null)
 * @method Afiliado|null findOneBy(array $criteria, array $orderBy = null)
 * @method Afiliado[]    findAll()
 * @method Afiliado[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AfiliadoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Afiliado::class);
    }
}
