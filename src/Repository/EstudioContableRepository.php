<?php

namespace App\Repository;

use App\Entity\EstudioContable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EstudioContable|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstudioContable|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstudioContable[]    findAll()
 * @method EstudioContable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstudioContableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EstudioContable::class);
    }
}
