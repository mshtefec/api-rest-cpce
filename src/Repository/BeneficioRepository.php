<?php

namespace App\Repository;

use App\Entity\Beneficio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Beneficio|null find($id, $lockMode = null, $lockVersion = null)
 * @method Beneficio|null findOneBy(array $criteria, array $orderBy = null)
 * @method Beneficio[]    findAll()
 * @method Beneficio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BeneficioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Beneficio::class);
    }
}
