<?php

namespace App\Repository;

use App\Entity\ObraSocial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ObraSocial|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObraSocial|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObraSocial[]    findAll()
 * @method ObraSocial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObraSocialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObraSocial::class);
    }
}
