<?php

namespace App\Repository;

use App\Entity\Titulo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Titulo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Titulo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Titulo[]    findAll()
 * @method Titulo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TituloRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Titulo::class);
    }
}
