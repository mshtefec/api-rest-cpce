<?php

namespace App\Repository;

use App\Entity\Procesotes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Procesotes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Procesotes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Procesotes[]    findAll()
 * @method Procesotes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcesotesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Procesotes::class);
    }
}
