<?php

namespace App\Repository;

use App\Entity\Cheques;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cheques|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cheques|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cheques[]    findAll()
 * @method Cheques[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChequesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cheques::class);
    }
}
