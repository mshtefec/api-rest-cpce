<?php

namespace App\Repository;

use App\Entity\Totales;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Totales|null find($id, $lockMode = null, $lockVersion = null)
 * @method Totales|null findOneBy(array $criteria, array $orderBy = null)
 * @method Totales[]    findAll()
 * @method Totales[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TotalesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Totales::class);
    }
}
