<?php

namespace App\Repository;

use App\Entity\Procesos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Procesos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Procesos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Procesos[]    findAll()
 * @method Procesos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcesosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Procesos::class);
    }
}
