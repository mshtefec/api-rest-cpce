<?php

namespace App\Repository;

use App\Entity\EstudiocontablesAfiliados;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EstudiocontablesAfiliados|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstudiocontablesAfiliados|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstudiocontablesAfiliados[]    findAll()
 * @method EstudiocontablesAfiliados[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstudiocontablesAfiliadosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EstudiocontablesAfiliados::class);
    }
}