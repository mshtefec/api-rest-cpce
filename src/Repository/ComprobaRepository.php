<?php

namespace App\Repository;

use App\Entity\Comproba;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comproba|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comproba|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comproba[]    findAll()
 * @method Comproba[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComprobaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comproba::class);
    }
}
