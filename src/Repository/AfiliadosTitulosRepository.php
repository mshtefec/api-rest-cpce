<?php

namespace App\Repository;

use App\Entity\AfiliadosTitulos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AfiliadosTitulos|null find($id, $lockMode = null, $lockVersion = null)
 * @method AfiliadosTitulos|null findOneBy(array $criteria, array $orderBy = null)
 * @method AfiliadosTitulos[]    findAll()
 * @method AfiliadosTitulos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AfiliadosTitulosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AfiliadosTitulos::class);
    }
}
